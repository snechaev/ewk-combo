#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
lsetup "views LCG_98python3 x86_64-centos7-gcc8-opt"
export PYTHONPATH=/cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/python:/cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/lib
source /afs/cern.ch/user/e/eballabe/venv-pyhf/bin/activate
cd /afs/cern.ch/user/e/eballabe/public/1L2L-combination
python script.py --m12 800 --m0 400 --factor 2.
