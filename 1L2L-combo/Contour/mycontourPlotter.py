#!/usr/bin/env python

# mycontourPlotter.py #################
import subprocess
import os
import ROOT
ROOT.gROOT.SetBatch()

#import SampleXS


class mycontourPlotter:
        def __init__(self, plotName="test", xSize=800, ySize=600):
                self.plotName = plotName
                self.xSize = xSize
                self.ySize = ySize
                self.canvas = ROOT.TCanvas(self.plotName,self.plotName,xSize,ySize)

                self.canvas.SetLeftMargin(0.13)
                self.canvas.SetRightMargin(0.2)#0.03)
                self.canvas.SetTopMargin(0.1)
                self.canvas.SetBottomMargin(0.15)

                self.xAxisLabel = "x label"
                self.yAxisLabel = "y label"

                self.processLabel = "Process Title -- Describe The Grid!"
                self.processLabel_position = (0.15,0.93)
                self.lumiLabel = "#sqrt{s}=XX TeV, YY fb^{-1}"
                self.lumiLabel_position = (0.15,0.93)
                self.limitCaveatLabel = "All limits at 95% CL"
                self.limitCaveatLabel_position = (0.15,0.93)
                self.ATLASLabel = "ATLAS"
                self.ATLASLabel_position = (0.15,0.93)
                self.PrelimLabel = "Internal"
                self.PrelimLabel_position = (0.25,0.93)
                self.anaLabel = "4 lepton"
                self.anaLabel_position = (0.15,0.93)

                self.bottomObject = 0
                self.legendObjects = []
                
                return

        def setXAxisLabel(self, label=""):
                self.xAxisLabel = label
                if self.bottomObject:
                	self.bottomObject.GetXaxis().SetTitle(self.xAxisLabel)
                return

        def setYAxisLabel(self, label=""):
                self.yAxisLabel = label
                if self.bottomObject:
                	self.bottomObject.GetYaxis().SetTitle(self.yAxisLabel)
                return

        def drawAxes(self, axisRange=[0,0,2000,2000]):
                self.canvas.cd()
                self.bottomObject = self.canvas.DrawFrame( *axisRange )
                self.bottomObject.SetTitle(";%s;%s"%(self.xAxisLabel,self.yAxisLabel) )
                self.bottomObject.GetYaxis().SetTitleOffset(1.2)
                self.bottomObject.GetYaxis().SetTitleSize(0.05)
                self.bottomObject.GetYaxis().SetLabelSize(0.05)
                self.bottomObject.GetXaxis().SetTitleOffset(1.3)
                self.bottomObject.GetXaxis().SetTitleSize(0.05)
                self.bottomObject.GetXaxis().SetLabelSize(0.05)
                if ("Wino" in self.plotName) or ("LV" in self.plotName) or ("GG" in self.plotName and "GGM" not in self.plotName):
                	self.bottomObject.GetYaxis().SetTitleOffset(1.5)
                	self.canvas.Update()
                return

        def drawOneSigmaBand(self, band, color=ROOT.TColor.GetColor("#ffd700"), alpha=0.3, legendOrder=0):
                self.canvas.cd()
                band.SetFillColorAlpha(color,alpha)
                band.SetFillStyle(1001)
                band.SetLineStyle(1)
                band.SetLineWidth(1)
                band.SetLineColorAlpha(ROOT.kGray,0.5)
                band.Draw("F")
                band.Draw("L")
                self.canvas.Update()
                tmpLegendObject = band.Clone("1SigmaBand")
                tmpLegendObject.SetLineColor(ROOT.kBlack)
                tmpLegendObject.SetLineStyle(7)
                tmpLegendObject.SetLineWidth(1)
                if type(legendOrder) == int:
                        self.legendObjects.append( ( legendOrder, tmpLegendObject, "Expected Limit (#pm1 #sigma_{exp})", "lf" ) )
                return

        def drawExpected(self, curve, color=ROOT.kBlack, alpha=0.9, legendOrder=None, title="Expected Limit", drawOption="L", width=4):
                self.canvas.cd()
                curve.SetLineColorAlpha(color,alpha)
                curve.SetLineStyle(7)
                curve.SetLineWidth(width)
                curve.Draw(drawOption)
                self.canvas.Update()
                if type(legendOrder) == int:
                	self.legendObjects.append( ( legendOrder, curve, title, "l" ) )
                return

        def drawObserved(self, curve, title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})", color=ROOT.TColor.GetColor("#800000"), alpha=0.7, legendOrder=1, drawOption="L", width=4):
                self.canvas.cd()
                curve.SetLineColorAlpha(color,alpha)
                curve.SetLineStyle(1)
                curve.SetLineWidth(width)
                #curve.Draw(drawOption)
                curve.Draw("L") 
                if drawOption == "LF" :
                    curve.SetFillColorAlpha(color,0.2)
                    curve.SetFillStyle(1001)
                    curve.Draw("F")
                self.canvas.Update()
                if type(legendOrder) == int:
                	self.legendObjects.append( ( legendOrder, curve.Clone("Observed"), title, "L" ) )
                return

        def drawTheoryUncertaintyCurve(self, curve, color=ROOT.TColor.GetColor("#800000"), alpha=0.7, style=3):
                self.canvas.cd()
                curve.SetLineColorAlpha(color,alpha)
                curve.SetLineStyle(style)
                curve.SetLineWidth(2)
                curve.Draw("L")
                self.canvas.Update()
                return

        def drawTextFromTGraph2D(self, graph, title="Grey numbers represent blah", color=ROOT.TColor.GetColor("#000000"), alpha=0.6, angle=30, size=0.015, format="%.1g", titlesize = 0.03):
                self.canvas.cd()
                tmpText = ROOT.TLatex()
                tmpText.SetTextSize(size)
                tmpText.SetTextColorAlpha(color,alpha)
                tmpText.SetTextAngle(angle)
                x,y,z = graph.GetX(), graph.GetY(), graph.GetZ()
                for i in range(len(x)):
                	tmpText.DrawLatex(x[i],y[i],format%z[i])

                tmpText.SetTextSize(titlesize)
                tmpText.SetTextAngle(-90)
                #tmpText.DrawLatexNDC(0.94,0.9,title)
                self.canvas.Update()
                return

        def drawShadedRegion(self, curve, color=ROOT.kGray, alpha=0.5, title="title", legendOrder=2):
                self.canvas.cd()
                curve.SetFillStyle(1001)
                curve.SetFillColorAlpha(color,alpha)
                curve.SetLineStyle(1)
                curve.SetLineWidth(1)
                curve.SetLineColorAlpha(ROOT.kGray,0.5)
                curve.Draw("F")
                curve.Draw("L")
                self.canvas.Update()
                if type(legendOrder) == int:
                	self.legendObjects.append( ( legendOrder, curve.Clone("ShadedRegion_"+title), title, "F" ) )
                return

        def drawLine(self, coordinates, label = "", color = ROOT.kGray+1, style = 7, labelLocation=0 , angle=0):
                self.canvas.cd()
                tmpLine = ROOT.TLine()
                tmpLine.SetLineColorAlpha(color,0.9)
                tmpLine.SetLineStyle(style)
                tmpLine.DrawLine(*coordinates)
                xmin,ymin,xmax,ymax = coordinates

                tmpLineLabel = ROOT.TLatex()
                tmpLineLabel.SetTextSize(0.028)
                tmpLineLabel.SetTextColor(color)
                tmpLineLabel.SetTextAngle(angle)
                if labelLocation:
                	tmpLineLabel.DrawLatex(labelLocation[0],labelLocation[1],label)
                else:
                	tmpLineLabel.DrawLatex(coordinates[0]+0.1*(coordinates[2]-coordinates[0]),
                					  coordinates[1]+0.16*(coordinates[3]-coordinates[1]),
                					  label)

                self.canvas.Update()
                return

        def decorateCanvas(self):
                self.canvas.cd()
                latexObject = ROOT.TLatex()
                latexObject.SetTextFont(42)
                latexObject.SetTextSize(0.045)
                latexObject.DrawLatexNDC(self.processLabel_position[0],self.processLabel_position[1],self.processLabel)

                latexObject.SetTextSize(0.04)
                latexObject.DrawLatexNDC(self.lumiLabel_position[0],self.lumiLabel_position[1], self.lumiLabel)
                latexObject.SetTextSize(0.025)
                latexObject.DrawLatexNDC(self.limitCaveatLabel_position[0],self.limitCaveatLabel_position[1], self.limitCaveatLabel)

                latexObject.SetTextSize(0.045)
                latexObject.DrawLatexNDC(self.anaLabel_position[0],self.anaLabel_position[1], self.anaLabel)
                		
                latexObject.SetTextFont(72)
                latexObject.SetTextSize(0.05)
                latexObject.DrawLatexNDC(self.ATLASLabel_position[0],self.ATLASLabel_position[1], self.ATLASLabel)
                		
                latexObject.SetTextFont(42)
                latexObject.SetTextSize(0.05)
                latexObject.DrawLatexNDC(self.PrelimLabel_position[0],self.PrelimLabel_position[1], self.PrelimLabel)

                		
                ROOT.gPad.SetTicks()
                ROOT.gPad.RedrawAxis()
                self.canvas.Update()
                return

        def createLegend(self, shape=(0.22,0.55,0.65,0.75) ):
                self.canvas.cd()
                legend=ROOT.TLegend(*shape)
                ROOT.SetOwnership( legend, 0 )
                legend.SetBorderSize(0)
                legend.SetFillStyle(0)
                legend.SetTextFont(42)
                legend.SetTextSize(0.04)

                self.legendObjects.sort(key=lambda x: x[0], reverse=True)
                for iItem, (legendOrder,item,title,style) in enumerate(self.legendObjects):
                	legend.AddEntry(item,title,style)

                return legend

        def drawTheoryLegendLines(self, xyCoord, length=0.05, ySeparation=0.026, color=ROOT.TColor.GetColor("#800000"), alpha=0.7, style=3 ):
                self.canvas.cd()
                tmpLine = ROOT.TLine()
                tmpLine.SetLineColorAlpha(color,alpha)
                tmpLine.SetLineStyle(style)
                tmpLine.SetLineWidth(2)
                tmpLine.DrawLineNDC(xyCoord[0],xyCoord[1],xyCoord[0]+length,xyCoord[1])
                tmpLine.DrawLineNDC(xyCoord[0],xyCoord[1]+ySeparation,xyCoord[0]+length,xyCoord[1]+ySeparation)

        def drawLegendStandard(self, shape=(0.22,0.55,0.65,0.75), dummycurve=None, lineColor=ROOT.kBlack, fillColor=ROOT.kGray ):
                self.canvas.cd()
                legendST=ROOT.TLegend(*shape)
                ROOT.SetOwnership( legendST, 0 )
                legendST.SetBorderSize(0)
                legendST.SetFillStyle(0)
                legendST.SetTextFont(42)
                legendST.SetTextSize(0.025)
                		
                curve1 = dummycurve.Clone("curve1")
                curve1.SetLineStyle(1)
                curve1.SetLineWidth(3)
                curve1.SetLineColorAlpha(lineColor,1)
                
                curve2 = dummycurve.Clone("curve2")
                curve2.SetLineStyle(7)
                curve2.SetLineWidth(3)
                curve2.SetLineColorAlpha(lineColor,1)
                if fillColor != None :
                    curve2.SetFillStyle(1001)
                    curve2.SetFillColorAlpha(fillColor,0.5)
                
                self.canvas.Update()
                if fillColor != None :
                    legendST.AddEntry(curve1,"#splitline{Observed Limit}{(#pm1 #sigma_{theory}^{SUSY})}","L")              
                    legendST.AddEntry(curve2,"#splitline{Expected Limit}{(#pm1 #sigma_{exp})}","FL")         		
                else :
                    legendST.AddEntry(curve1,"Observed Limit","L")              
                    legendST.AddEntry(curve2,"Expected Limit","L")         		
                legendST.Draw()
                self.canvas.Update()
                if fillColor != None :
                    self.drawTheoryLegendLines(xyCoord=(shape[0]+0.01,shape[3]-0.048), length=0.033, color=lineColor )

        def drawLegendSpecial(self, shape=(0.80,0.20,1,0.7), dummycurve=None, linecolors=[], bandcolors=[], labels=[]):
                self.canvas.cd()
                legendLLE=ROOT.TLegend(*shape)
                ROOT.SetOwnership( legendLLE, 0 )
                legendLLE.SetBorderSize(0)
                legendLLE.SetFillStyle(0)
                legendLLE.SetTextFont(42)
                legendLLE.SetTextSize(0.025)

                curves = []
                for i,lab in enumerate(labels):
                        curve = dummycurve.Clone("curve_"+str(i))
                        curve.SetLineStyle(1)
                        curve.SetLineWidth(4)
                        curve.SetLineColorAlpha(linecolors[i],0.7)
                        if bandcolors != None :
                            curve.SetFillStyle(1001)
                            curve.SetFillColorAlpha(bandcolors[i],0.3)
                            if "Combination" in lab: curve.SetFillColorAlpha(bandcolors[i],0.5)
                        curves.append(curve)
                        
                self.canvas.Update()
                for i,curve in enumerate(curves):
                    if bandcolors != None :
                        legendLLE.AddEntry(curve,labels[i],"FL")
                    else :
                        legendLLE.AddEntry(curve,labels[i],"L")
                legendLLE.Draw()
                self.canvas.Update()
                return 
                
        def writePlot(self, format="pdf"):
	        if not os.path.isdir("pdf"):
		        os.makedirs("pdf")
	        if not os.path.isdir("eps"):
		        os.makedirs("eps")
	        self.canvas.SaveAs("PAPER_plots/"+self.plotName+".pdf")
	        #self.canvas.SaveAs("eps/"+self.plotName+".eps")
	        #self.canvas.SaveAs("png/"+self.plotName+".png")
	        return

        def writePlotCLs(self, curve, ExpObs="Exp", subname="", filename="", xmax=1000,ymax=1000):
                self.canvas.cd()

                mytext = []
                for x in range(0, curve.GetN()):
                        if curve.GetX()[x]>xmax or curve.GetY()[x]>ymax:
                            continue
                        totxs  = 1.
                        if ExpObs=="xsUL":
                            print("ERROR: I haven't adapted the script to work with 'xsUL' yet. See L293 of myContourPlotter.py")
                            sys.exit(1)
                            #totxs = SampleXS.getXSfb(curve.GetX()[x],"GGM")
                        mytext_temp = ROOT.TText(curve.GetX()[x], curve.GetY()[x], '{:.1f}'.format(curve.GetZ()[x]*totxs))
                        mytext_temp.SetTextSize(0.01)
                        mytext_temp.SetTextColorAlpha(ROOT.kBlack,0.9)
                        #mytext_temp.SetTextAngle(45)
                        print(ExpObs, curve.GetX()[x], curve.GetY()[x], '{:.1f}'.format(curve.GetZ()[x]*totxs) )		
                        mytext.append(mytext_temp)
                for i in mytext:
                        i.Draw()		
                        
                self.canvas.Update()
                tmpText = ROOT.TLatex()
                tmpText.SetTextSize(0.028)
                tmpText.SetTextAngle(+90)
                tmpText.SetTextColorAlpha(ROOT.kBlack,0.9)
                myextraText = ""
                if ExpObs=="xsUL": myextraText = "Numbers give 95% excluded model cross-sections [fb] "
                if ExpObs=="CLsExp": myextraText = "Numbers give expected 95% CLs values "
                if ExpObs=="CLsObs": myextraText = "Numbers give observed 95% CLs values "
                tmpText.DrawLatexNDC(0.99,0.15,myextraText)
                				
                self.canvas.Update()
                self.canvas.SaveAs("pdf/"+self.plotName+subname+"_"+ExpObs+".pdf")
                self.canvas.SaveAs("eps/"+self.plotName+subname+"_"+ExpObs+".eps")
                #self.canvas.SaveAs("png/"+self.plotName+subname+"_"+ExpObs+".png")
                		
                return
