import sys
import re
import json

def extract_numerical_values(file_path):
    with open(file_path, 'r') as file:
        content = file.read()

    # Extract numerical values using regular expressions
    numerical_values = re.findall(r"(?<!sigma\))[-+]?\d*\.\d+e[+-]?\d+|(?<!sigma\))\d*\.\d+|(?<!sigma\))\d+", content)

    # Convert the values to floats, excluding specific values
    numerical_values = [float(value) for value in numerical_values if value not in {'-2', '-1', '1', '2'}]

    return numerical_values

def generate_json_output(file_path):
    # Extract numerical values from the file
    values = extract_numerical_values(file_path)

    # Define keys for JSON output
    keys = [
        "m0", "m12", "CLs", "clsd2s", "clsd1s", "CLsexp", "clsu1s", "clsu2s",
        "covqual", "dodgycov", "excludedXsec", "expectedUpperLimit",
        "expectedUpperLimitMinus1Sig", "expectedUpperLimitMinus2Sig",
        "expectedUpperLimitPlus1Sig", "expectedUpperLimitPlus2Sig", "fID",
        "failedcov", "failedfit", "failedp0", "failedstatus", "fitstatus",
        "mode", "nexp", "nofit", "p0", "p0d1s", "p0d2s", "p0exp", "p0u1s", "p0u2s",
        "p1", "seed", "sigma0", "sigma1", "upperLimit", "upperLimitEstimatedError",
        "xsec", "x", "y"
    ]

    # Define default values for other keys
    default_values = {
        "CLs": None,
        "CLsexp": None,
        "clsd1s": None,
        "clsd2s": None,
        "clsu1s": None,
        "clsu2s": None,
        "covqual": 3.0,
        "dodgycov": 0.0,
        "excludedXsec": -999007.0,
        "expectedUpperLimit": -1.0,
        "expectedUpperLimitMinus1Sig": -1.0,
        "expectedUpperLimitMinus2Sig": -1.0,
        "expectedUpperLimitPlus1Sig": -1.0,
        "expectedUpperLimitPlus2Sig": -1.0,
        "fID": -1.0,
        "failedcov": 0.0,
        "failedfit": 0.0,
        "failedp0": 1.0,
        "failedstatus": 0.0,
        "fitstatus": 0.0,
        "mode": -1.0,
        "nexp": -1.0,
        "nofit": 0.0,
        "p0": 0.5,
        "p0d1s": -1.0,
        "p0d2s": -1.0,
        "p0exp": -1.0,
        "p0u1s": -1.0,
        "p0u2s": -1.0,
        "p1": 0.5,
        "seed": 0.0,
        "sigma0": -1.0,
        "sigma1": -1.0,
        "upperLimit": -1.0,
        "upperLimitEstimatedError": -1.0,
        "xsec": -999007.0,
        "x": None,
        "y": None
    }

    # Create a dictionary with default values
    data = default_values.copy()

    # Update the dictionary with extracted values
    data.update(dict(zip(keys, values)))

    # Set "x" and "y" equal to "m0" and "m12"
    data["x"] = data["m0"]
    data["y"] = data["m12"]

    # Convert the dictionary to JSON format
    json_output = json.dumps(data, indent=2)

    # Print or save the JSON output as needed
    print(json_output)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <filename>")
        sys.exit(1)

    file_path = sys.argv[1]
    generate_json_output(file_path)
