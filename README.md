# ewk-combo
This repo contains scripts to make additional electroweak combination studies.

Current version of CLs_functions.py is compatibale with pyhf 0.5.2

More detailed information about pyhf can be found under this [link](https://pyhf.readthedocs.io/en/v0.5.2/index.html).

# Scan of BRs for N2 decaying to either a Z or a H boson
```
cd BRZH
python3 Main.py
```


# Instruction for the installation of pyhf 0.5.2 (at lxplus.cern.ch)

```
python3 -m venv pyhf_0_5_2
source pyhf_0_5_2/bin/activate
python -m pip install -I pyhf==0.5.2
python -m pip install  pyhf[backends]==0.5.2

```
A common error which can occur during running `python  Main.py ` is  related to unexpected location of defs.json etc.

Proposed solution is following:

```
cd pyhf_0_5_2/lib/python3.7/site-packages/pyhf/schemas
ln -s 1.0.0/defs.json  defs.json
ln -s 1.0.0/jsonpatch.json  jsonpatch.json
ln -s 1.0.0/measurement.json measurement.json
ln -s 1.0.0/model.json  model.json 
ln -s 1.0.0/patchset.json patchset.json
ln -s 1.0.0/workspace.json workspace.json

```

If you have questions, please contact eric.ballabene@cern.ch and serafima.nechaeva@cern.ch
