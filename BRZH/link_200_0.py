import os
from scripts.init_run_functions import *

mass_pt_str = '200_0'
combo_list = [ 'WZ_0l', 
                'Wh_0l',
                # 'WZ_3l', 
                'Wh_3l',]
path_source = '/gpfs_data/local/atlas/serafima/combo_data'
link_files(mass_pt_str, combo_list, path_source)
