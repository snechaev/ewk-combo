import numpy as np
import json
import time
from CLs_functions import CLs_0l_combined
from CLs_functions import CLs_3l_combined
from CLs_functions import CLs_full_combination
from CLs_functions import CLs_0l1lbb2l2j3l_combination
from CLs_functions import CLs_0l1lbb3l_combination


mass_point = [200, 50]
br = 30

print('mass_point:', mass_point)
print('br = ', br)

print('CLs_0l1lbb3l_combination:')
time_st0 = time.time()
out = CLs_0l1lbb3l_combination(br, mass_point)
time_st1 = time.time()
print(time_st1 - time_st0, 'sec')

print('------------------------------------------')

print('CLs_0l1lbb2l2j3l_combination:')
time_st2 = time.time()
out = CLs_0l1lbb2l2j3l_combination(br, mass_point)
time_st3 = time.time()
print(time_st3 - time_st2, 'sec')
print('------------------------------------------')

print('CLs_full_combination:')
time_st4 = time.time()
out = CLs_full_combination(br, mass_point)
time_st5 = time.time()
print(time_st5 - time_st4, 'sec')
