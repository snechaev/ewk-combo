import os

# create catalogues for the point m1_m2
def mk_dirs(mass_pt_str, path):
    mass_pt_dir = os.path.join(path, mass_pt_str)
    os.system('mkdir ' + mass_pt_dir)

    condor_jobs_dir = os.path.join(mass_pt_dir, 'condor_jobs')
    run_scripts_dir = os.path.join(mass_pt_dir, 'run_scripts')
    br_outputs_dir = os.path.join(mass_pt_dir, 'br_outputs')

    os.system('mkdir ' + condor_jobs_dir)
    os.system('mkdir ' + run_scripts_dir)
    os.system('mkdir ' + br_outputs_dir)

    logs_dir = os.path.join(condor_jobs_dir, 'log')
    errors_dir = os.path.join(condor_jobs_dir, 'error')
    outouts_scripts_dir = os.path.join(condor_jobs_dir, 'output')

    os.system('mkdir ' + logs_dir)
    os.system('mkdir ' + errors_dir)
    os.system('mkdir ' + outouts_scripts_dir)

# generate a set of job_brNN.sub
def new_sub(br, mass_pt_str, point_path, type_combo):
    source_path = '../../queue_condor_runs/templates'
    source = os.path.join(source_path, 'job_template.sub')
    condor_jobs_dir = os.path.join(point_path, 'condor_jobs')
    new_file = os.path.join(condor_jobs_dir, 'job_br'+str(br)+'.sub')

    with open (source, 'r') as f_source:
        template = f_source.read()

    content = template.replace('$BR_VALUE', str(br))
    content = content.replace('$MASS_PT', mass_pt_str)
    content = content.replace('$COMBO_TYPE', type_combo)

    with open (new_file, 'w') as f_new:
        f_new.write(content)
    return new_file

# generate a set of run_brNN.sh
def new_sh(br, mass_pt_str, point_path, type_combo):
    source_path = '../../queue_condor_runs/templates'
    source = os.path.join(source_path, 'run_template.sh')
    run_scripts_dir = os.path.join(point_path, 'run_scripts')
    new_file = os.path.join(run_scripts_dir, 'run_br'+str(br)+'.sh')

    with open (source, 'r') as f_source:
        template = f_source.read()

    content = template.replace('$BR_VALUE', str(br))
    content = content.replace('$MASS_PT', mass_pt_str)
    content = content.replace('$COMBO_TYPE', type_combo)

    with open (new_file, 'w') as f_new:
        f_new.write(content)



from scripts.link_files import construct_name
from scripts.link_files import link_files
from scripts.link_files import construct_input

def check_combo(mass_point_str, combo_list, path_source):
    answer = True
    for combo in combo_list: 
        if construct_name(mass_point_str, combo, path_source) == None:
            answer = answer * False
        else:
            answer = answer * True
    # print(mass_point_str, answer)
    return answer


