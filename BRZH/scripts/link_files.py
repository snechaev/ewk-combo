import os

# mass_point_string = '500_0'

# combos_to_link = [
#                 'WZ_0l',
#                 'Wh_0l',
#                 'WZ_2l2j',
#                 'WZ_3l',
#                 'Wh_3l',
#                  ]

# cnaf
# source_path = '/gpfs_data/local/atlas/serafima/combo_data/EWKcombo_ANA-SUSY-2020-05/'
# source_path = '/Users/serafima/cernbox/SUSY/pyhf_studies/combo_data/EWKcombo_ANA-SUSY-2020-05'

# 0l:
# /gpfs_data/local/atlas/serafima/combo_data/EWKcombo_ANA-SUSY-2020-05/AllHad-SUSY-2018-41/Workspaces/

# Sig_sig_SM_C1N2_WZ_500_0.json
# Sig_sig_SM_C1N2_Wh_500_0.json

# 3l:
# /gpfs_data/local/atlas/serafima/combo_data/EWKcombo_ANA-SUSY-2020-05/3LOnshell-SUSY-2019-09/Workspaces/

# CN_WZ_500_0_harmonised_onshell.json (или SMAwz13TeV_500_0_combined_NormalMeasurement_model.json) ???
# Wh/RECAST/
# workspace_3L_500p0_0p0.json

# 2l2j:
# /gpfs_data/local/atlas/serafima/combo_data/EWKcombo_ANA-SUSY-2020-05/2L2J-SUSY-2018-05/Workspaces/C1N2_WZ_2L2J/
#  C1N2_WZ_500p0_0p0_2L2J.json



def construct_name(mass_pts_str, combo_type, source_path):
    mass_pts_str_list = mass_pts_str.split('_')
    mass_pts_p = mass_pts_str_list[0]+'p0_'+mass_pts_str_list[1]+'p0'
    file_path = None
    if combo_type == 'WZ_0l':
        path = os.path.join(source_path, 'EWKcombo_ANA-SUSY-2020-05', 'AllHad-SUSY-2018-41', 'Workspaces')
        file_name = 'Sig_sig_SM_C1N2_WZ_'+mass_pts_str+'.json'
        file_path = os.path.join(path, file_name)
        if os.path.exists(file_path) == False:
            new_path = os.path.join(source_path, 
                        'EWK0L_ANA-SUSY-2018-41/Results_unblind/v4.1/for_combination/C1N2_WZ/workspace_json')
            file_name = 'Sig_sig_SM_C1N2_WZ_'+mass_pts_str+'.json'
            file_path = os.path.join(new_path, file_name)
            if os.path.exists(file_path) == False:
                new_path = os.path.join(source_path, 
                        'EWK0L_ANA-SUSY-2018-41/Results_unblind/v3.4/for_combination/C1N2_WZ/workspace_json')
                # print(new_path)
                file_name = 'Sig_sig_SM_C1N2_WZ_'+mass_pts_str+'.json'
                file_path = os.path.join(new_path, file_name)
                if os.path.exists(file_path) == False:
                        new_path = os.path.join(source_path, 'Workspaces-RECAST-2023/0L')
                        # Sig_sig_C1N2_WZ_400_200_0_0
                        file_name = 'Sig_sig_C1N2_WZ_'+mass_pts_str+'_0_0.json'
                        file_path = os.path.join(new_path, file_name)
                        # print(file_path)
                        if os.path.exists(file_path) == False:
                            file_path = None
                        else:
                            print("\n\nWe are here (WZ)\n\n")
                
    if combo_type == 'Wh_0l':
        path = os.path.join(source_path, 'EWKcombo_ANA-SUSY-2020-05', 'AllHad-SUSY-2018-41', 'Workspaces')
        file_name = 'Sig_sig_SM_C1N2_Wh_'+mass_pts_str+'.json'
        file_path = os.path.join(path, file_name)
        if os.path.exists(file_path) == False:
            path = os.path.join(path, 'Wh_RECAST')
            # workspace_0L_450p0_0p0
            file_name = 'workspace_0L_'+mass_pts_p+'.json'
            file_path = os.path.join(path, file_name)
            # if os.path.exists(file_path) == False:
            #     path = os.path.join(path, 'not_used')
            #     file_name = 'workspace_0L_'+mass_pts_p+'.json'
            #     file_path = os.path.join(path, file_name)
            if os.path.exists(file_path) == False:
                new_path = os.path.join(source_path, 
                    'EWK0L_ANA-SUSY-2018-41/Results_unblind/v4.1/for_combination/C1N2_Wh/workspace_json')
                # print(new_path)
                file_name = 'Sig_sig_SM_C1N2_Wh_'+mass_pts_str+'.json'
                file_path = os.path.join(new_path, file_name)
                if os.path.exists(file_path) == False:
                    new_path = os.path.join(source_path, 
                    'EWK0L_ANA-SUSY-2018-41/Results_unblind/v3.4/for_combination/C1N2_Wh/workspace_json')
                    file_name = 'Sig_sig_SM_C1N2_Wh_'+mass_pts_str+'.json'
                    file_path = os.path.join(new_path, file_name)
                    if os.path.exists(file_path) == False:
                        new_path = os.path.join(source_path, 'Workspaces-RECAST-2023/0L')
                        # Sig_sig_C1N2_Wh_900_700_0_0
                        file_name = 'Sig_sig_C1N2_Wh_'+mass_pts_str+'_0_0.json'
                        file_path = os.path.join(new_path, file_name)
                        # print(file_path)
                        if os.path.exists(file_path) == False:
                            file_path = None
                        else:
                            print("\n\nWe are here (Wh)\n\n")
                


    if combo_type == 'WZ_2l2j':
        path = os.path.join(source_path,'EWKcombo_ANA-SUSY-2020-05', '2L2J-SUSY-2018-05', 'Workspaces', 'C1N2_WZ_2L2J')
        file_name = 'C1N2_WZ_'+mass_pts_p+'_2L2J.json'
        file_path = os.path.join(path, file_name)
        if os.path.exists(file_path) == False:
            file_path = None
    if combo_type == 'WZ_3l':
        path = os.path.join(source_path, 'EWKcombo_ANA-SUSY-2020-05', '3LOnshell-SUSY-2019-09', 'Workspaces')
        file_name = 'SMAwz13TeV_'+mass_pts_str+'_combined_NormalMeasurement_model.json' 
        file_path = os.path.join(path, file_name)
        if os.path.exists(file_path) == False:
            file_name = 'CN_WZ_'+mass_pts_str+'_harmonised_onshell.json'
            file_path = os.path.join(path, file_name)
            if os.path.exists(file_path) == False:
                new_path = os.path.join(source_path,
                 'EWK3L_ANA-SUSY-2018-06/CombinationInputs/onshell/WZ-CONF-2020-015/likelihoods')
                # SMAwz13TeV_800_150_combined_NormalMeasurement_model
                file_name = 'SMAwz13TeV_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
                file_path = os.path.join(new_path, file_name)
                # print(file_name)
                if os.path.exists(file_path) == False:
                    file_path = None
                else:
                    print('new')
    if combo_type == 'Wh_3l':
        path = os.path.join(source_path, 'EWKcombo_ANA-SUSY-2020-05', '3LOnshell-SUSY-2019-09', 'Workspaces', 'Wh')
        # SMAwh13TeV_425_0_combined_NormalMeasurement_model
        file_name = 'SMAwh13TeV_' + mass_pts_str +'_combined_NormalMeasurement_mode.json'
        file_path = os.path.join(path, file_name)
        # print('searching for:\n', file_path)
        if os.path.exists(file_path) == False:
            path = os.path.join(path, 'RECAST')
            file_name = 'workspace_3L_'+mass_pts_p+'.json'
            file_path = os.path.join(path, file_name)
            # print('searching for:\n', file_path)
            if os.path.exists(file_path) == False:
                new_path = os.path.join(source_path,
                 'EWK3L_ANA-SUSY-2018-06/CombinationInputs/onshell/Wh-CONF-2020-015/likelihoods')
                # SMAwz13TeV_800_150_combined_NormalMeasurement_model
                file_name = 'SMAwh13TeV_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
                file_path = os.path.join(new_path, file_name)
                if os.path.exists(file_path) == False:
                    file_path = None

    if combo_type == 'Wh_1lbb':
        path = os.path.join(source_path, 'EWKcombo_ANA-SUSY-2020-05/1Lbb-SUSY-2019-08/Workspaces/RECAST')
        # Wh_1Lbb_recast_300p0_100p0.json
        file_name = 'Wh_1Lbb_recast_'+mass_pts_p+'.json'
        file_path = os.path.join(path, file_name)
        if os.path.exists(file_path) == False:
                print('\nCheck Wh 1bb pathset!!\n')
                file_path = None
    # file_path = os.path.join(path, file_name)
    return file_path



def construct_input(mass_pts_str, combo_type):
    mass_pts_str_list = mass_pts_str.split('_')
    if combo_type == 'WZ_0l':
        file_name = 'C1N2WZ_0L_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
    if combo_type == 'Wh_0l':
        file_name = 'C1N2Wh_0L_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
    if combo_type == 'WZ_2l2j':
        file_name = '2L2J_C1N2_WZ_'+mass_pts_str_list[0]+'p0_'+mass_pts_str_list[1]+'p0_2L2J.json'
    if combo_type == 'WZ_3l':
        file_name = 'C1N2WZ_3L_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
        # file_name = 'SMAwz13TeV_'+mass_pts_str+'combined_NormalMeasurement_model.json' 
    if combo_type == 'Wh_3l':
        file_name = 'C1N2Wh_3L_' +mass_pts_str+'_combined_NormalMeasurement_model.json'
    if combo_type == 'Wh_1lbb':
        file_name = 'C1N2Wh_1Lbb_'+mass_pts_str+'_combined_NormalMeasurement_model.json'
    return file_name



def link_files(mass_pts_str, combos_list, source_path):
    print('linking files')
    print(combos_list)
    for combo in combos_list:
        print(combo)
        print('link for point', mass_pts_str, combo)
        file_source = construct_name(mass_pts_str, combo, source_path)
        if file_source == None:
            print('It is not possible to use combo', combo, '\t Source for this poit does not exist!!!')
            print('Link will not be created!')
            continue
        file_input = construct_input(mass_pts_str, combo)
        # print('It is a test\t', 'ln -s '+file_source+' ' + file_input)
        os.system('ln -s '+ file_source + ' ' + file_input)

def remove_links():
    os.system('find -maxdepth 1 -type l -delete')

# link_files(mass_point_string, combos_to_link, source_path)

