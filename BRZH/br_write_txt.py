# import rerun_job
import os
import numpy as np
from CLs_functions import CLs_0l_combined
from CLs_functions import CLs_3l_combined
from CLs_functions import CLs_full_combination
from CLs_functions import CLs_0l1lbb3l_combination
from CLs_functions import CLs_0l3l_no_patch1lbb_combo
from CLs_functions import CLs_0l1lbb2l2j3l_combination
import argparse

parser = argparse.ArgumentParser()
parser.add_argument ('-m', '--mass_point', dest='mass_point', type=str, help='Mass point m1_m2')
parser.add_argument ('-b', '--br', dest='br', type=float, help='branching ratio (from 0 to 100)')
parser.add_argument('--combo_type', dest='combo_type', type=str,
 choices=['CLs_0l', 'CLs_3l', 'CLs_combo0l3l_full', 'CLs_0l1lbb3l', 'CLs_0l3l_no_patch1lbb', 'CLs_0l1lbb2l2j3l'],
  help='Which combo to calculate?')
args = parser.parse_args()
br = args.br
mass_pt_str = args.mass_point
combo_type = args.combo_type


def dict_CLS(out_CLs, br):
    pts_dict = dict()
    names = ['CLs', 'CLexp_0', 'CLexp_plus', 'CLexp_minus']
    for name in names:
        pts_dict[name] = dict()
    pts_dict['CLs'][str(br)] = float(out_CLs['CLs_obs'])
    pts_dict['CLexp_0'][str(br)] = float(out_CLs['CLs_exp_0sigma'])
    pts_dict['CLexp_plus'][str(br)] = float(out_CLs['CLs_exp_+1sigma'])
    pts_dict['CLexp_minus'][str(br)] = float(out_CLs['CLs_exp_-1sigma'])
    return pts_dict

path_pyhf_st = '/home/ATLAS-T3/serafima/SUSY/pyhf_studies'

path_txts_pt1 = os.path.join('queue_condor_runs', combo_type, mass_pt_str, 'br_outputs')
path_txts = os.path.join(path_pyhf_st, path_txts_pt1)
mass_pt = [int(mass) for mass in mass_pt_str.split('_')]

if combo_type == 'CLs_0l':
    out = CLs_0l_combined(br, mass_pt)

if combo_type == 'CLs_3l':
    out = CLs_3l_combined(br, mass_pt)

if combo_type == 'CLs_combo0l3l_full':
    out = CLs_full_combination(br, mass_pt)

if combo_type == 'CLs_0l1lbb3l':
    out = CLs_0l1lbb3l_combination(br, mass_pt)

if combo_type == 'CLs_0l3l_no_patch1lbb':
    out = CLs_0l3l_no_patch1lbb_combo(br, mass_pt)

if combo_type == 'CLs_0l1lbb2l2j3l':
    out = CLs_0l1lbb2l2j3l_combination(br, mass_pt)

output_br = str(dict_CLS(out, br))


# write in a file
txt_file_write = os.path.join(path_txts, mass_pt_str + '_br' +str(br)+'.txt')
with open(txt_file_write , 'w', encoding='utf-8') as f1:
    f1.write(output_br)
del f1
