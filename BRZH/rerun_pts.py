import rerun_job

mass_pt_str = '400_0'
combo_types = ['CLs_0l', 'CLs_combo0l3l']
for combo_type in combo_types:
    rerun_job.rerun_missing(mass_pt_str, combo_type)
