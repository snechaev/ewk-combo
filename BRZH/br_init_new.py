import os
from scripts.init_run_functions import *

mass_pt = [400, 0]
# possible options for combo_type: ['CLs_0l', 'CLs_3l', 'CLs_combo0l3l_full', 'CLs_0l1lbb3l', 'CLs_0l3l_no_patch1lbb']

#combo_type = 'CLs_0l1lbb3l'
combo_type = 'CLs_0l1lbb2l2j3l'

br_list = []
for br_i in range(0, 101, 5):
    br_list.append(br_i)

br_list.append(99.9)
br_list = [0,]

path_source = '/gpfs_data/local/atlas/serafima/combo_data'
# path_source = '/Users/serafima/cernbox/SUSY/pyhf_studies/combo_data/'

combo_dict = {
    'CLs_0l':   [
                'WZ_0l', 
                'Wh_0l',
                ],
    'CLs_3l':   [
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_combo0l3l_full':  [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_0l1lbb3l': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_0l3l_no_patch1lbb': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                'Wh_1lbb',
                ],
    'CLs_0l1lbb2l2j3l': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                'WZ_2l2j'
                ],
    'custom': [
                # 'WZ_0l', 
                # 'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                'WZ_2l2j'
                ],
            }



print('br list:', br_list)

mass_pt_str = str(mass_pt[0])+'_'+str(mass_pt[1])
combo_list = combo_dict[combo_type]

print('Running script for the point:', mass_pt_str)
print('combo:', combo_type)
print(combo_list)

#assert check_combo(mass_pt_str, combo_list, path_source), "Combo is not full, aborting..."
#print('Ok, the point will work, creating the links...')

link_files(mass_pt_str, combo_list, path_source)
path = os.path.join('../../queue_condor_runs', combo_type)
os.system('mkdir ' + path)

mk_dirs(mass_pt_str, path)
point_path = os.path.join(path, mass_pt_str)

# a set of branching ratios in range 0 - 100 % 
for br in br_list:
    submit_file = new_sub(br, mass_pt_str, point_path, combo_type)
    new_sh(br, mass_pt_str, point_path, combo_type)
    os.system('condor_submit '+ submit_file)

