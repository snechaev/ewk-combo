import numpy as np
import json
from CLs_functions import CLs_0l_combined

# def construct_name(mass_pt):
#     pass

# file_WZ = "C1N2WZ_0L_600_100_combined_NormalMeasurement_model.json"
# file_Wh = "C1N2Wh_0L_600_100_combined_NormalMeasurement_model.json"

def construct_empty_dict(N_pts):
    pts_dict = dict()
    names = ['CLs', 'CLexp_0', 'CLexp_plus', 'CLexp_minus']
    for name in names:
        pts_dict[name] = dict()
    pts_dict['N'] = N_pts
    return pts_dict

def add_CLS(dict_pts, out_CLs, br):
    # CLs_obs = out['CLs_obs']
    # CLexp_0 = out['CLs_exp_0sigma']
    # CLexp_plus = out['CLs_exp_+1sigma']
    # CLexp_minus = out['CLs_exp_-1sigma']
    dict_pts['CLs'][str(br)] = float(out['CLs_obs'])
    dict_pts['CLexp_0'][str(br)] = float(out['CLs_exp_0sigma'])
    dict_pts['CLexp_plus'][str(br)] = float(out['CLs_exp_+1sigma'])
    dict_pts['CLexp_minus'][str(br)] = float(out['CLs_exp_-1sigma'])



mass_pt = [300, 100]
N = 101
#N = 51
pts_dict = construct_empty_dict(N)
br_values = np.linspace(0, 100, N)
# labels_in = ['CLs_obs', 'CLs_exp_0sigma', 'CLs_exp_+1sigma', 'CLs_exp_-1sigma']
# labels_out = ['CLs_obs', 'CLexp_0', 'CLexp_plus', 'CLexp_minus']
k = 0
for br in br_values:
    k = k + 1
    print('measurement', k, ', br =', br)
    out = CLs_0l_combined(br, mass_pt)
    print("out:", out)
    add_CLS(pts_dict, out, round(br/100, 2))

# write to json file
jsonname = 'Plots/300_100_0l.json'
with open(jsonname, 'w', encoding='utf-8') as fh:
    fh.write(json.dumps(pts_dict, ensure_ascii=False)) 
