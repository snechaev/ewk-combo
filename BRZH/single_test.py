import os
from scripts.init_run_functions import *
import numpy as np
from CLs_functions import CLs_0l_combined
from CLs_functions import CLs_3l_combined
from CLs_functions import CLs_full_combination
from CLs_functions import CLs_0l1lbb3l_combination
from CLs_functions import CLs_0l3l_no_patch1lbb_combo
from CLs_functions import CLs_0l1lbb2l2j3l_combination


path_source = '/gpfs_data/local/atlas/serafima/combo_data'
#path_source = '/Users/serafima/cernbox/SUSY/pyhf_studies/combo_data/'
br = 0
mass_pt = [400, 0]
#combo_type = 'custom'
#combo_type = 'CLs_0l3l_no_patch1lbb'
combo_type = 'CLs_0l1lbb2l2j3l'

# possible options for combo_type: ['CLs_0l', 'CLs_3l', 'CLs_combo0l3l_full', 'CLs_0l1lbb3l']

combo_dict = {
    'CLs_0l':   [
                'WZ_0l', 
                'Wh_0l',
                ],
    'CLs_3l':   [
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_combo0l3l_full':  [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_0l1lbb3l': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                ],
    'CLs_0l3l_no_patch1lbb': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                'Wh_1lbb',
                ],
    'CLs_0l1lbb2l2j3l': [
                'WZ_0l', 
                'Wh_0l',
                'WZ_3l', 
                'Wh_3l',
                'WZ_2l2j'
                ],
    'custom': [
                #'WZ_0l', 
                #'Wh_0l',
                #'WZ_3l', 
                #'Wh_3l',
                #'WZ_2l2j',
                'Wh_1lbb',
                ],
            }



br_list = []
for br_i in range(0, 101, 20):
    br_list.append(br_i)


mass_pt_str = str(mass_pt[0])+'_'+str(mass_pt[1])


combo_list = combo_dict[combo_type]

print('Running script for the point:', mass_pt_str)
print('combo:', combo_type)
print(combo_list)

assert check_combo(mass_pt_str, combo_list, path_source), "Combo is not full, aborting..."
print('Ok, the point will work, creating the links...')

link_files(mass_pt_str, combo_list, path_source)


# path = os.path.join('../../queue_condor_runs', combo_type)
# os.system('mkdir ' + path)

# mk_dirs(mass_pt_str, path)
# point_path = os.path.join(path, mass_pt_str)





if combo_type == 'CLs_0l':
    print('running combo', combo_type)
    out = CLs_0l_combined(br, mass_pt)

if combo_type == 'CLs_3l':
    print('running combo', combo_type)
    out = CLs_3l_combined(br, mass_pt)

if combo_type == 'CLs_combo0l3l_full':
    print('running combo', combo_type)
    out = CLs_full_combination(br, mass_pt)

if combo_type == 'CLs_0l1lbb3l':
    print('running combo', combo_type)
    out = CLs_0l1lbb3l_combination(br, mass_pt)

if combo_type == 'CLs_0l3l_no_patch1lbb':
    print('running combo', combo_type)
    out =  CLs_0l3l_no_patch1lbb_combo(br, mass_pt)

if combo_type == 'CLs_0l1lbb2l2j3l':
    print('running combo', combo_type)
    out = CLs_0l1lbb2l2j3l_combination(br, mass_pt)


print(out)
