import pyhf ; pyhf.set_backend("numpy")
import json
import copy
import numpy as np



def make_workspace(data):
    workspace = pyhf.Workspace(data) #forms pyhf workspace from json data file
    return workspace

#This function makes a pyhf workspace from data (dict object from json file)








def get_signal_data(data,channel_name):
    channels_data = data.get('channels')
    for i in range(len(channels_data)):
        if channels_data[i].get('name') == channel_name: #finding the channel 
            index_channel = i #storing index
            break
        else:
            continue
    
    channel_OI = channels_data[index_channel]
    samples_OI = channel_OI.get('samples')
    for j in range(len(samples_OI)):
        if {'data': None, 'name': 'mu_SIG', 'type': 'normfactor'} in samples_OI[j].get('modifiers'): #searching samples for the signal sample
            index_sample = j
            break
        else:
            continue
    
    data_value = samples_OI[index_sample].get('data')[0]
    return data_value, index_channel, index_sample


#This function retrieves the signal sample information for a given data (dict object from json file) for a specific channel
#channel name is string object, being the name of a channel in the data
#outputs:  signal nominal yield ,  which channel (index), and which sample (index)









def check_for_signal(channel_data):
    samples = channel_data.get('samples')
    ret = False
    for j in range(len(samples)):
        mods = samples[j].get('modifiers')
        if {'data': None, 'name': 'mu_SIG', 'type': 'normfactor'} in mods:
            ret = True
            break
        else:
            continue
    return ret


#This function gives a True/False output, determining if a given channel_data which is a dictionary object, has a signal sample
# True => has a signal sample
# False => channel has no signal sample








def apply_br(data,br):
    channels = data.get('channels')
    for i in range(len(channels)):
        #check for signal in channel
        if check_for_signal(channels[i]) : #will only apply Br change if True that there is a signal sample in the channel
            name = channels[i].get('name')
            data_value, index_channel, index_sample = get_signal_data(data,name) #get all relevant signal sample information
            data.get('channels')[index_channel].get('samples')[index_sample]['data'] = [data_value*br]
            mods = data.get('channels')[index_channel].get('samples')[index_sample]['modifiers']
            for j in range(len(mods)): 
                if mods[j].get('type') == 'histosys' : #apply br to histosys modifiers too
                    hi = mods[j].get('data')['hi_data'][0]
                    lo = mods[j].get('data')['lo_data'][0]
                    data.get('channels')[index_channel].get('samples')[index_sample].get('modifiers')[j]['data'] = {'hi_data': [br*hi], 'lo_data': [br*lo]}
            continue
        
        else:
            continue
        
        
        
    return data

#Takes the data (dict object from json file), and applies the branching ratio to the signal sample
#Output is data object with adjusted signal samples











def overlap(workspace1,workspace2,channel_overlap):
    W1 = copy.copy(workspace1) ; W2 = copy.copy(workspace2)
    
    #need to place a check here
    #that both channels have 
    channels1  = W1.get('channels')
    for i in range(len(channels1)):
        name  = channels1[i].get('name') 
        if name == channel_overlap:
            channels1_index = i
            break
        else:
            continue
        continue
    
    channels2  = W2.get('channels')
    for i in range(len(channels2)):
        name  = channels2[i].get('name') 
        if name == channel_overlap:
            channels2_index = i
            break
        else:
            continue
        continue
        
    samples1 = channels1[channels1_index].get('samples')
    for i in range(len(samples1)):
        name = samples1[i].get('name')
        if name == 'signal_sample':
            signal1_index = i
            break
        else:
            continue
        continue
        
    samples2 = channels2[channels2_index].get('samples')
    for i in range(len(samples2)):
        name = samples2[i].get('name')
        if name == 'signal_sample':
            signal2_index = i
            break
        else:
            continue
        continue            
        
    
    
    #now can get signal samples
    S1 = samples1[signal1_index]
    S2 = samples2[signal2_index]
    
    #now need a loop that combines the two to refrom signal sample1, and re-enter it into workspace 1
    #needs to deal with yields and mods
    
    #change of nominal yield
    data1 = S1.get('data')[0]
    data2 = S2.get('data')[0]
    new_data = data1 + data2
    W1.get('channels')[channels1_index].get('samples')[signal1_index]['data'] = [new_data]
    
    #now to deal with the modifiers ; need to deal with case by case
    mods1 = S1.get('modifiers')
    mods2 = S2.get('modifiers')
    
    mods1_names = []
    for i in range(len(mods1)):
        name = mods1[i].get('name')
        mods1_names.append(name)
        
    
    for i in range(len(mods2)):
        mod = mods2[i]
        mod_type = mod['type']
        if mod_type == 'lumi':
            continue
        if mod_type == 'normfactor':
            continue
            
        if mod_type == 'histosys':
            name = mod.get('name')
            if name in mods1_names:
                mod1 = mods1[mods1_names.index(name)]
                if mod1.get('type') == 'histosys':
                    hi_data_1 = mod1.get('data')['hi_data'][0] ; lo_data_1 = mod1.get('data')['lo_data'][0]
                    hi_data_2 = mod.get('data')['hi_data'][0] ;  lo_data_2 = mod.get('data')['lo_data'][0]
                    hi_new = hi_data_1 + hi_data_2
                    lo_new = lo_data_1 + lo_data_2
                    W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['hi_data'] = [hi_new]
                    W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['lo_data'] = [lo_new]

                else:
                    hi_data_1 = mod1.get('data')['hi'];
                    lo_data_1 = mod1.get('data')['lo']
                    if data2 == 0.0:
                        hi_data_2 = 0.0
                        lo_data_2 = 0.0
                    else:
                        hi_data_2 = mod.get('data')['hi_data'][0]/data2; 
                        lo_data_2 = mod.get('data')['lo_data'][0]/data2;
                        
                    hi_new = hi_data_1 + hi_data_2
                    lo_new = lo_data_1 + lo_data_2
                    W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['hi_data'] = [hi_new]
                    W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['lo_data'] = [lo_new]

            else: 
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'].append(mod)

                continue
                
        if mod_type == 'normsys':
            name = mod.get('name')
            if name in mods1_names:
                mod1 = mods1[mods1_names.index(name)]
                hi_data_1 = mod1.get('data')['hi'] -1 ; lo_data_1 = mod1.get('data')['lo'] -1
                hi_data_2 = mod.get('data')['hi'] -1; lo_data_2 = mod.get('data')['lo'] -1
                hi_new = hi_data_1*(data1/new_data) + hi_data_2*(data2/new_data)
                lo_new = lo_data_1*(data1/new_data) + lo_data_2*(data2/new_data)
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['hi'] = hi_new +1 
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)].get('data')['lo'] = lo_new +1

            else: 
                hi_data_1 = mod.get('data')['hi'] ; lo_data_1 = mod.get('data')['lo']
                hi_new = hi_data_1*(data2/new_data)
                lo_new = lo_data_1*(data2/new_data)
                mod.get('data')['hi'] = hi_new
                mod.get('data')['lo'] = lo_new
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'].append(mod)

                continue
        
        if mod_type == 'staterror':
            name = mod.get('name')
            if name in mods1_names:
                mod1 = mods1[mods1_names.index(name)]
    
                stat1 = mod1['data'][0]
                stat2 = mod['data'][0]
                stat_new = np.sqrt(
                    ((stat1)**2)*((data1/new_data)**2)
                    +
                    ((stat2)**2)*((data2/new_data)**2)
                    )
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'][mods1_names.index(name)]['data'] = [stat_new]

            else:
                stat1 = mod1['data'][0]
                stat_new = stat1*(data1/new_data)
                mod['data'] = [stat_new]
                W1.get('channels')[channels1_index].get('samples')[signal1_index]['modifiers'].append(mod)

    return W1,W2


#This function accounts for common channels in a workspace, where only signal sample different
#shifts all signal sample information onto the workspace W1, (specified by channel_overlap, which is a string of the channels name)
#all signal sample information for the channel_overlap for W1 and W2 contained in signal sample for the channel in W1
#Then prior to W1, W2 combination, prune channel_overlap from W2, to prevent double counting
#output is W1 with all signal information from w1 and w2 w.r.t channel_overlap, and the original w2








def apply_br_patchset(patchset,br,mass_point):

    masses = (mass_point[0],mass_point[1]) #turning mass_point into list object to select appropriate patch
        
        
    patches = patchset[masses].patch    #get patches for this mass_point 
    for i in range(len(patches)):
        nominal_array = patchset[masses].patch[i].get('value')['data'] #get nominal yields
        for j in range(len(nominal_array)):
            patchset[masses].patch[i].get('value')['data'][j] = br*nominal_array[j] #for each NY in the nominal_array, apply branching ratio
        
        mods = patchset[masses].patch[i].get('value').get('modifiers') #apply br to modifiers
        for j in range(len(mods)):
            mod = mods[j]
            if mod.get('type') == 'histosys':
                nominal = mod.get('data')
                hi_data = nominal.get('hi_data')
                lo_data = nominal.get('lo_data')
                for k in range(len(hi_data)):
                    hi_data[k] = hi_data[k]*br
                    lo_data[k] = lo_data[k]*br
                
                
                patchset[masses].patch[i].get('value').get('modifiers')[j]['data']['hi_data'] = hi_data
                patchset[masses].patch[i].get('value').get('modifiers')[j]['data']['lo_data'] = lo_data
                continue
        
            else:
                continue

    return patchset


#Takes patchset (pyhf object) and adjusts signal according to the branching ratio br
#important that br here is BR(Wh) as a decimal
#mass_point is array object, i.e. [200,50
#Outputs adjusted patchset









def renaming_3l0l_workspaces(workspaceZ,workspaceh):
    workspaceh = workspaceh.rename(modifiers = { #renaming degenerate mods within 0l basis
        'JET_EffectiveNP_4' : 'JET_EffectiveNP_4_2B2Q' ,
        'JET_EffectiveNP_6' : 'JET_EffectiveNP_6_2B2Q' ,
        'JET_EffectiveNP_7' : 'JET_EffectiveNP_7_2B2Q' ,
        'JET_CombMass_Baseline': 'JET_CombMass_Baseline_2B2Q' ,
        'JET_CombMass_Modelling' : 'JET_CombMass_Modelling_2B2Q' ,
        'JET_CombMass_TotalStat' : 'JET_CombMass_TotalStat_2B2Q' ,
        'JET_CombMass_Tracking1' : 'JET_CombMass_Tracking1_2B2Q' ,
        'JET_CombMass_Tracking2' : 'JET_CombMass_Tracking2_2B2Q' ,
        'JET_CombMass_Tracking3' : 'JET_CombMass_Tracking3_2B2Q' ,
        'JET_EffectiveNP_8restTerm' : 'JET_EffectiveNP_8restTerm_2B2Q' ,
        'JET_EffectiveNP_R10_Detector1' : 'JET_EffectiveNP_R10_Detector1_2B2Q' ,
        'JET_EffectiveNP_R10_Detector2' : 'JET_EffectiveNP_R10_Detector2_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed1' : 'JET_EffectiveNP_R10_Mixed1_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed2' : 'JET_EffectiveNP_R10_Mixed2_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed3' : 'JET_EffectiveNP_R10_Mixed3_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed4' : 'JET_EffectiveNP_R10_Mixed4_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling1' : 'JET_EffectiveNP_R10_Modelling1_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling2' : 'JET_EffectiveNP_R10_Modelling2_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling3' : 'JET_EffectiveNP_R10_Modelling3_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling4' : 'JET_EffectiveNP_R10_Modelling4_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical1' : 'JET_EffectiveNP_R10_Statistical1_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical2' : 'JET_EffectiveNP_R10_Statistical2_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical3' : 'JET_EffectiveNP_R10_Statistical3_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical4' : 'JET_EffectiveNP_R10_Statistical4_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical5' : 'JET_EffectiveNP_R10_Statistical5_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical6' : 'JET_EffectiveNP_R10_Statistical6_2B2Q' ,
        'JET_EtaIntercalibration_R10_TotalStat' : 'JET_EtaIntercalibration_R10_TotalStat_2B2Q' ,
        'JET_LargeR_TopologyUncertainty_top' : 'JET_LargeR_TopologyUncertainty_top_2B2Q' ,
        'JET_LargeR_TopologyUncertainty_V' : 'JET_LargeR_TopologyUncertainty_V_2B2Q'
        
        
    }) 
    workspaceh = workspaceh.rename(modifiers = { #renaming degenerate mods within combined basis
    'EG_SCALE_ALL':'EG_Scale',
    'EG_Scale':'EG_SCALE_ALL_2B2Q',
    'bTagWeight_FT_EFF_B_systematics':'syst_FT_Eff_B',
    'bTagWeight_FT_EFF_C_systematics':'syst_FT_Eff_C',
    'bTagWeight_FT_EFF_Light_systematics':'syst_FT_Eff_L',
    'syst_jer_1':'JET_JER_EffectiveNP_1',
    'syst_jer_1':'JET_JER_EffectiveNP_1_2B2Q',
    'syst_jer_2':'JET_JER_EffectiveNP_2',
    'syst_jer_2':'JET_JER_EffectiveNP_2_2B2Q',
    'syst_jer_3':'JET_JER_EffectiveNP_3',
    'syst_jer_3':'JET_JER_EffectiveNP_3_2B2Q',
    'syst_jer_4':'JET_JER_EffectiveNP_4',
    'syst_jer_4':'JET_JER_EffectiveNP_4_2B2Q',
    'syst_jer_5':'JET_JER_EffectiveNP_5',
    'syst_jer_5':'JET_JER_EffectiveNP_5_2B2Q',
    'syst_jer_6':'JET_JER_EffectiveNP_6',
    'syst_jer_6':'JET_JER_EffectiveNP_6_2B2Q',
    'syst_jer_7':'JET_JER_EffectiveNP_7restTerm',
    'syst_jer_7':'JET_JER_EffectiveNP_7restTerm_2B2Q',
    'syst_flav_comp':'JET_Flavor_Composition',
    'syst_flav_comp':'JET_Flavor_Composition_2B2Q',
    'syst_flav_resp':'JET_Flavor_Response',
    'syst_flav_resp':'JET_Flavor_Response_2B2Q',
    'syst_etaInter_mod':'JET_EtaIntercalibration_Modelling',
    'syst_etaInter_mod':'JET_EtaIntercalibration_Modelling_2B2Q',
    'syst_etaInter_NC2018':'JET_EtaIntercalibration_NonClosure_2018data',
    'syst_etaInter_NC2018':'JET_EtaIntercalibration_NonClosure_2018data_2B2Q',
    'syst_etaInter_posEta':'JET_EtaIntercalibration_NonClosure_posEta',
    'syst_etaInter_posEta':'JET_EtaIntercalibration_NonClosure_posEta_2B2Q',
    'syst_etaInter_stat':'JET_EtaIntercalibration_TotalStat',
    'syst_etaInter_stat':'JET_EtaIntercalibration_TotalStat_2B2Q',
    'syst_PU_offsetMu':'JET_Pileup_OffsetMu_2B2Q',
    'syst_PU_offsetMu':'JET_Pileup_OffsetMu',
    'syst_PU_offsetNPV':'JET_Pileup_OffsetNPV',
    'syst_PU_offsetNPV':'JET_Pileup_OffsetNPV_2B2Q',
    'syst_PU_pt':'JET_Pileup_PtTerm',
    'syst_PU_pt':'JET_Pileup_PtTerm_2B2Q',
    'syst_PU_rho':'JET_Pileup_RhoTopology',
    'syst_PU_rho':'JET_Pileup_RhoTopology_2B2Q',
    'syst_jvtSF':'jvtWeight_JET_JvtEfficiency',
    'ttbar_isrfsr':'syst_Theory_ttbar_ISR',
    'ttbar_ME':'syst_Theory_ttbar_ME',
    'ttbar_pdf':'syst_Theory_ttbar_PDF',
    'ttbar_PS':'syst_Theory_ttbar_PS'

        
    })
    workspaceZ = workspaceZ.rename(modifiers = { #renaming degenerate mods within 0l basis
        'JET_EffectiveNP_4' : 'JET_EffectiveNP_4_2B2Q' ,
        'JET_EffectiveNP_5' : 'JET_EffectiveNP_5_2B2Q' ,
        'JET_EffectiveNP_6' : 'JET_EffectiveNP_6_2B2Q' ,
        'JET_EffectiveNP_7' : 'JET_EffectiveNP_7_2B2Q' ,
        'JET_CombMass_Baseline': 'JET_CombMass_Baseline_2B2Q' ,
        'JET_CombMass_Modelling' : 'JET_CombMass_Modelling_2B2Q' ,
        'JET_CombMass_TotalStat' : 'JET_CombMass_TotalStat_2B2Q' ,
        'JET_CombMass_Tracking1' : 'JET_CombMass_Tracking1_2B2Q' ,
        'JET_CombMass_Tracking2' : 'JET_CombMass_Tracking2_2B2Q' ,
        'JET_CombMass_Tracking3' : 'JET_CombMass_Tracking3_2B2Q' ,
        'JET_EffectiveNP_8restTerm' : 'JET_EffectiveNP_8restTerm_2B2Q' ,
        'JET_EffectiveNP_R10_Detector1' : 'JET_EffectiveNP_R10_Detector1_2B2Q' ,
        'JET_EffectiveNP_R10_Detector2' : 'JET_EffectiveNP_R10_Detector2_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed1' : 'JET_EffectiveNP_R10_Mixed1_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed2' : 'JET_EffectiveNP_R10_Mixed2_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed3' : 'JET_EffectiveNP_R10_Mixed3_2B2Q' ,
        'JET_EffectiveNP_R10_Mixed4' : 'JET_EffectiveNP_R10_Mixed4_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling1' : 'JET_EffectiveNP_R10_Modelling1_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling2' : 'JET_EffectiveNP_R10_Modelling2_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling3' : 'JET_EffectiveNP_R10_Modelling3_2B2Q' ,
        'JET_EffectiveNP_R10_Modelling4' : 'JET_EffectiveNP_R10_Modelling4_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical1' : 'JET_EffectiveNP_R10_Statistical1_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical2' : 'JET_EffectiveNP_R10_Statistical2_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical3' : 'JET_EffectiveNP_R10_Statistical3_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical4' : 'JET_EffectiveNP_R10_Statistical4_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical5' : 'JET_EffectiveNP_R10_Statistical5_2B2Q' ,
        'JET_EffectiveNP_R10_Statistical6' : 'JET_EffectiveNP_R10_Statistical6_2B2Q' ,
        'JET_EtaIntercalibration_R10_TotalStat' : 'JET_EtaIntercalibration_R10_TotalStat_2B2Q' ,
        'JET_LargeR_TopologyUncertainty_top' : 'JET_LargeR_TopologyUncertainty_top_2B2Q' ,
        'JET_LargeR_TopologyUncertainty_V' : 'JET_LargeR_TopologyUncertainty_V_2B2Q'
        
        
    })
    workspaceZ = workspaceZ.rename(modifiers = {   #renaming degenerate mods within combined basis
        
         'EG_SCALE_ALL':     'EG_Scale',
         'EG_Scale' :      'EG_SCALE_ALL_2B2Q',
         'bTagWeight_FT_EFF_B_systematics' : 'syst_FT_Eff_B',
         'bTagWeight_FT_EFF_C_systematics' :      'syst_FT_Eff_C',
         'bTagWeight_FT_EFF_Light_systematics' :       'syst_FT_Eff_L',
         'syst_jer_1' :     'JET_JER_EffectiveNP_1' ,
         'syst_jer_1'  :     'JET_JER_EffectiveNP_1_2B2Q' ,
         'syst_jer_2'  :     'JET_JER_EffectiveNP_2' ,
         'syst_jer_2' :        'JET_JER_EffectiveNP_2_2B2Q' ,
         'syst_jer_3' :        'JET_JER_EffectiveNP_3' ,
         'syst_jer_3' :        'JET_JER_EffectiveNP_3_2B2Q' ,
         'syst_jer_4':'JET_JER_EffectiveNP_4' ,
         'syst_jer_4':'JET_JER_EffectiveNP_4_2B2Q' ,
         'syst_jer_5':'JET_JER_EffectiveNP_5' ,
         'syst_jer_5':'JET_JER_EffectiveNP_5_2B2Q' ,
         'syst_jer_6':'JET_JER_EffectiveNP_6' ,
         'syst_jer_6':'JET_JER_EffectiveNP_6_2B2Q' ,
         'syst_jer_7':'JET_JER_EffectiveNP_7restTerm' ,
         'syst_jer_7':'JET_JER_EffectiveNP_7restTerm_2B2Q' ,
         'syst_flav_comp':'JET_Flavor_Composition' ,
         'syst_flav_comp':'JET_Flavor_Composition_2B2Q' ,
         'syst_flav_resp':'JET_Flavor_Response' ,
         'syst_flav_resp':'JET_Flavor_Response_2B2Q' ,
         'syst_etaInter_mod':'JET_EtaIntercalibration_Modelling' ,
         'syst_etaInter_mod':'JET_EtaIntercalibration_Modelling_2B2Q' ,
         'syst_etaInter_NC2018':'JET_EtaIntercalibration_NonClosure_2018data' ,
         'syst_etaInter_NC2018':'JET_EtaIntercalibration_NonClosure_2018data_2B2Q' ,
         'syst_etaInter_posEta':'JET_EtaIntercalibration_NonClosure_posEta' ,
         'syst_etaInter_posEta':'JET_EtaIntercalibration_NonClosure_posEta_2B2Q' ,
         'syst_etaInter_stat':'JET_EtaIntercalibration_TotalStat' ,
         'syst_etaInter_stat':'JET_EtaIntercalibration_TotalStat_2B2Q' ,
         'syst_PU_offsetMu':'JET_Pileup_OffsetMu_2B2Q' ,
         'syst_PU_offsetMu':'JET_Pileup_OffsetMu' ,
         'syst_PU_offsetNPV':'JET_Pileup_OffsetNPV' ,
         'syst_PU_offsetNPV':'JET_Pileup_OffsetNPV_2B2Q' ,
         'syst_PU_pt':'JET_Pileup_PtTerm' ,
         'syst_PU_pt':'JET_Pileup_PtTerm_2B2Q' ,
         'syst_PU_rho':'JET_Pileup_RhoTopology' ,
         'syst_PU_rho':'JET_Pileup_RhoTopology_2B2Q' ,
         'syst_jvtSF':'jvtWeight_JET_JvtEfficiency' ,

        
    })    
    
    return workspaceZ,workspaceh

#This function accounts for modifiers in the workspaces that have different names for the same modifier
#Inputs are the WZ and Wh workspaces for the combined 3l and 0l data sets
#Outputs WZ and Wh workspaces with renamed modifiers















""" CLs calculator functions

The following functions generate a CLs value for a specific combination scenario, for specific parameters

Throughout test stat qtilde is used (see --> arXiv:1007.1727)

Confidence level is CLs for mu = 1.0, signal scenario (see arXiv:hep-ex/9902006v1 eqn. 6 for definition, or https://pyhf.readthedocs.io/en/v0.6.0/_generated/pyhf.infer.hypotest.html?highlight=hypotest)


Input:
    br is branching ratio of the WZ scenario as a percentage (i.e. 50%): BR(WZ) = br/100 , BR(Wh) = (1 - br)/100

    mass_point input selects the SUSY model of interest:
        mass_point = array including the mass of the light neutralino and the mass of the heavy linos [mass of heavy linos (int), mass of neutralino (int)]  -- i.e., mass_point = [200,50]


Output:
    A dictionary with labelled data points:
            {'CLs_obs':,    'CLs_exp_0sigma':,     'CLs_exp_-1sigma':,      'CLs_exp_+1sigma': }
"""






def CLs_0l_combined(br, mass_point):
    
    #Convert array definition of mass_point into string, such that can select data files to use
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    br_z = br/100 #turning branching ratio for WZ from a percentage into a decimal
    
    
    #using string form of mass point, now selecting the data files of interest
    file_OI_3 = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_OI_4 = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ;
    
    
    #loading in the WZ data
    data_z = json.load(open(file_OI_3)) #load in json data as a dictionary object
    data_z = apply_br(data_z,br_z) #modify signal samples in the data to account for branching ratio
    workspace_WZ = make_workspace(data_z) #form pyhf workspace from adjusted data
    workspace_WZ = pyhf.workspace.Workspace.prune(workspace_WZ, channels = ['CR2B2Q_cuts']) #removing a control region, as obsolete once combined with Wh
    workspace_WZ = workspace_WZ.rename(measurements={'NormalMeasurement': 'WZ_Measurement'})  #require different measurement names when combining workspaces
    
        
    #loading in the Wh data
    data_h = json.load(open(file_OI_4))   #load in json data as a dictionary object
    data_h = apply_br(data_h,1.0-br_z) #modify signal samples in the data to account for branching ratio
    workspace_Wh = make_workspace(data_h)
    workspace_Wh = workspace_Wh.rename(measurements={'NormalMeasurement': 'Wh_Measurement'})
    
    # workspace_WZ, workspace_Wh = overlap(workspace_WZ, workspace_Wh, channel_overlap='CR2B2Q_cuts')
    # workspace_WZ = pyhf.workspace.Workspace.prune(workspace_WZ, channels = ['CR2B2Q_cuts'])
    
    workspace = pyhf.Workspace.combine(workspace_WZ,workspace_Wh) #combining Wh and WZ workspaces to form new combined workspace
    #note signal regions in this case are adjoint so don't need to account for overlap
    model = workspace.model()
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True) #generate CLs values, observed and expected bounds
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output







def CLs_3l_combined(br, mass_point):
    
    #Convert array definition of mass_point into string, such that can select data files to use
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    br_z = br/100 #turning branching ratio for WZ from a percentage into a decimal
    
    
    

    
    file_OI_1 = 'C1N2WZ_3L_'+mass_point_string+'_combined_NormalMeasurement_model.json' ; 
    file_OI_2 = 'C1N2Wh_3L_'+mass_point_string+'_combined_NormalMeasurement_model.json';
    data_z = json.load(open(file_OI_1)) #loading data as dict object
    data_h = json.load(open(file_OI_2))
    data_z = apply_br(data_z,br_z) #applying branching ratio to data
    data_h = apply_br(data_h,1.0-br_z)
    W1 = make_workspace(data_z) #WZ workspace 
    W2 = make_workspace(data_h) #Wh workspace
    W1 = W1.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    W2 = W2.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'})


        
    
    CR_channels = ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts'];  #control regions common, not adjoint
    #in the combined workspace want just one CR for each of the above, with the signal sample the combinaiton of the WZ and Wh signal samples
    for k in range(len(CR_channels)):
        channel_OI = CR_channels[k] 
        W1,W2 = overlap(W1,W2,channel_OI) #all W2 signal sample data accounted for now in W1
    

    W2 = pyhf.workspace.Workspace.prune(W2, channels = CR_channels) #remove CRs from Wh workspace, so in combination have just one of each CR with all info needed within
    W1 = W1.rename(measurements={'NormalMeasurement': 'WZ_Measurement'}) #cannot have common measurement name when combining
    W2 = W2.rename(measurements={'NormalMeasurement': 'Wh_Measurement'})

    workspace = pyhf.Workspace.combine(W1,W2) #combined workspaces
    
    model = workspace.model()
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True) #generating CLs values
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output

def CLs_0l3l_combo(br, mass_point):
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    br_z = br/100

    file_0l_WZ = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_0l_Wh = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ;
    
    file_3l_WZ = 'C1N2WZ_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_3l_Wh = 'C1N2Wh_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json';

    data_0l_z = json.load(open(file_0l_WZ)) #loading data as dict object
    data_0l_h = json.load(open(file_0l_Wh))
    data_3l_z = json.load(open(file_3l_WZ)) #loading data as dict object
    data_3l_h = json.load(open(file_3l_Wh))

    data_0l_z = apply_br(data_0l_z, br_z)
    data_0l_h = apply_br(data_0l_h, 1.0 - br_z)
    data_3l_z = apply_br(data_3l_z, br_z)
    data_3l_h = apply_br(data_3l_h, 1.0 - br_z)
# ....................
    workspace_Wh_0l = make_workspace(data_0l_h)
    workspace_WZ_0l = make_workspace(data_0l_z)
    workspace_Wh_3l = make_workspace(data_3l_h)
    workspace_WZ_3l = make_workspace(data_3l_z)

#  ...................

    workspace_Wh_0l = workspace_Wh_0l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_0l'})
    workspace_WZ_0l = workspace_WZ_0l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_0l'})


    workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'})


# ....................

    # overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    # for k in range(len(overlap_channels)):
    #     channel_OI = overlap_channels[k] #selecting channel to apply overlap
    #     workspace_WZ, workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)


    # workspace_Wh_0l = pyhf.workspace.Workspace.prune(workspace_Wh_0l, channels = overlap_channels)
    workspace_WZ_0l = pyhf.workspace.Workspace.prune(workspace_WZ_0l, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.

# ....................
    CR_channels = ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts'];  #control regions common, not adjoint
    #in the combined workspace want just one CR for each of the above, with the signal sample the combinaiton of the WZ and Wh signal samples
    for k in range(len(CR_channels)):
        channel_OI = CR_channels[k] 
        workspace_WZ_3l,workspace_Wh_3l = overlap(workspace_WZ_3l, workspace_Wh_3l, channel_OI) #all W2 signal sample data accounted for now in W1
    

    workspace_Wh_3l = pyhf.workspace.Workspace.prune(workspace_Wh_3l, channels = CR_channels)

# .................... 

    workspace_Wh_3l = workspace_Wh_3l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_3l'})
    workspace_WZ_3l = workspace_WZ_3l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_3l'})


    workspace_0l = pyhf.Workspace.combine(workspace_WZ_0l, workspace_Wh_0l)
    workspace_3l = pyhf.Workspace.combine(workspace_WZ_3l, workspace_Wh_3l)
    workspace = pyhf.Workspace.combine(workspace_0l, workspace_3l)
    # workspace = pyhf.Workspace.combine(workspace_bkgd_only_1lbb,workspace,join = 'left outer')
    # workspace = pyhf.Workspace.combine(workspace,workspace_twolep,join = 'left outer')


     #########################################
    #have now combined all the workspaces

# ..............................................



# ..............................................

    # modifier_settings={
    #     "normsys": {"interpcode": "code4"},
    #     "histosys": {"interpcode": "code4p"},
    # }
    # model = workspace.model(
    #     patches=[patchset[patch_name]],
    #     modifier_settings=modifier_settings
    # )
    model = workspace.model()  
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output







def CLs_1lbb(mass_point):
    
    file1 = 'C1N2Wh_1Lbb_bkgonly.json' # file for the background samples
    file2 = 'C1N2Wh_1Lbb_patchset.json' #file for the signal patches
    
    spec = json.load(open(file1))
    patchset = pyhf.PatchSet( json.load(open(file2)) )
    
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino) #mass_point array into string
    patch_OI = 'C1N2_Wh_hbb_' + mass_point_string #specifying which signal sample will be added
    workspace = pyhf.Workspace(spec) # background only workspace
    model =  workspace.model( patches=[patchset[patch_OI]] ) #constructed pyhf model with signal sample as a patch
    
    data = workspace.data(model) #data loaded in for the hypotest to use
    
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output







def CLs_full_combination(br, mass_point):
    
    br_z = br/100 #percentage branching ratio to decimal
    
    #turn mass_point into useable string
    m_light_neutralino = mass_point[1]; m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    
    
    
    
    
    #Loading in the 1lbb data and forming bkgd. workspace
    file1 = 'C1N2Wh_1Lbb_bkgonly.json' # file for the background samples
    file2 = 'C1N2Wh_1Lbb_patchset.json' #file for the signal patches
    
    spec = json.load(open(file1))
    patchset = pyhf.PatchSet( json.load(open(file2)) ) 
    workspace_bkgd_only_1lbb = pyhf.Workspace(spec) # background only workspace
    
    
    
    
    
    
    
    
    #Loading and adjusting the WZ/Wh data, which is premade to be a combination of 3l and 0l
    Wh_file = '3l0l_h_' + mass_point_string + '.json'
    WZ_file = '3l0l_Z_' + mass_point_string + '.json'
    
    data_WZ = json.load(open(WZ_file)) #load json file
    data_WZ = apply_br(data_WZ, br_z) #adjust data according to branching ratio
    workspace_WZ = make_workspace(data_WZ) 
    
    data_Wh = json.load(open(Wh_file)) #load json file
    data_Wh = apply_br(data_Wh, 1.0 - br_z) #adjust data according to branching ratio
    workspace_Wh = make_workspace(data_Wh)
    
    #rename modifiers in the WZ and Wh workspaces
    #This is correlation of modifiers.
    workspace_WZ,workspace_Wh = renaming_3l0l_workspaces(workspace_WZ,workspace_Wh)
    
    
    
    
    
    
    
    
    
    
    #Giving all signal samples the same name
    Z_sig_name = 'SMAwz13TeV_' + mass_point_string #prev. signal names
    h_sig_name = 'SMAwh13TeV_' + mass_point_string
    workspace_WZ = workspace_WZ.rename(samples = {Z_sig_name:'signal_sample'}) #renaming so one signal sample name throughout
    workspace_Wh = workspace_Wh.rename(samples = {h_sig_name:'signal_sample'})
    
    Z_sig_name = 'sig_SM_C1N2_WZ_' + mass_point_string
    h_sig_name = 'sig_SM_C1N2_Wh_' + mass_point_string
    workspace_WZ = workspace_WZ.rename(samples = {Z_sig_name: 'signal_sample'})
    workspace_Wh = workspace_Wh.rename(samples = {h_sig_name: 'signal_sample'}) 








    #need different measurement names when combining
    workspace_WZ = workspace_WZ.rename(measurements={'NormalMeasurement': 'WZ_Measurement'}) 
    workspace_Wh = workspace_Wh.rename(measurements={'NormalMeasurement': 'Wh_Measurement'})

    







    #Account for common channels
    overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    for k in range(len(overlap_channels)):
        channel_OI = overlap_channels[k] #selecting channel to apply overlap
        workspace_WZ,workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)
    #overlap sets workspace_WZ so that all workspace_Wh signal addition for the overlapping channel
    #of interest is in that channel for workspace WZ, then channel pruned from workspace_Wh
        
    workspace_Wh = pyhf.workspace.Workspace.prune(workspace_Wh, channels = overlap_channels)
    workspace_WZ = pyhf.workspace.Workspace.prune(workspace_WZ, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.
    


    workspace = pyhf.Workspace.combine(workspace_WZ,workspace_Wh)
    workspace = pyhf.Workspace.combine(workspace_bkgd_only_1lbb,workspace,join = 'left outer')
    #have now combined all the workspaces
    
    
    #patching in the 1lbb signal data
    patch_name = 'C1N2_Wh_hbb_' + mass_point_string #which mass point signal contribution we are adding
    patchset = apply_br_patchset(patchset,1.0-br_z,mass_point) #apply the specifc branching ratio to the signal sample
    modifier_settings={
        "normsys": {"interpcode": "code4"},
        "histosys": {"interpcode": "code4p"},
    }
    model = workspace.model(
        patches=[patchset[patch_name]],
        modifier_settings=modifier_settings
    )
    
    
    
    
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output













def CLs_2l(mass_point):
    #Convert array definition of mass_point into string, such that can select data files to use
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'p0_'+str(m_light_neutralino)+'p0'
    
    file = '2L2J_C1N2_WZ_' + mass_point_string + '_2L2J.json'
    data = json.load(open(file))
    workspace = pyhf.Workspace(data)

    model = workspace.model()
    data = workspace.data(model)
    #pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer())
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True) #generate CLs values, observed and expected bounds

    print(CLs_obs)
    print(CLs_exp)
    

def CLs_2l_with_br(br,mass_point):
    #Convert array definition of mass_point into string, such that can select data files to use
    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'p0_'+str(m_light_neutralino)+'p0'
    
    br_z = br/100 #turning branching ratio for WZ from a percentage into a decimal

    file = '2L2J_C1N2_WZ_' + mass_point_string + '_2L2J.json'
    data = json.load(open(file))
    data = apply_br(data,br_z) #modify signal samples in the data to account for branching ratio

    workspace = pyhf.Workspace(data)

    model = workspace.model()
    data = workspace.data(model)
    #pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer())
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True) #generate CLs values, observed and expected bounds

    print(CLs_obs)
    print(CLs_exp)
    



def CLs_my_full_combination(br, mass_point):
    
    br_z = br/100 #percentage branching ratio to decimal
    
    #turn mass_point into useable string
    m_light_neutralino = mass_point[1]; m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    
    mass_point_string_twolep = str(m_heavy_linos)+'p0_'+str(m_light_neutralino)+'p0'
    filetwolep = '2L2J_C1N2_WZ_' + mass_point_string_twolep + '_2L2J.json'
    datatwolep = json.load(open(filetwolep))
    datatwolep = apply_br(datatwolep,br_z) #modify signal samples in the data to account for branching ratio
    workspace_twolep = make_workspace(datatwolep) 



    #Loading in the 1lbb data and forming bkgd. workspace
    file1 = 'C1N2Wh_1Lbb_bkgonly.json' # file for the background samples
    file2 = 'C1N2Wh_1Lbb_patchset.json' #file for the signal patches
    
    spec = json.load(open(file1))
    patchset = pyhf.PatchSet( json.load(open(file2)) ) 
    workspace_bkgd_only_1lbb = pyhf.Workspace(spec) # background only workspace
    
    
    #Loading and adjusting the WZ/Wh data, which is premade to be a combination of 3l and 0l
    Wh_file = '3l0l_h_' + mass_point_string + '.json'
    WZ_file = '3l0l_Z_' + mass_point_string + '.json'
    
    data_WZ = json.load(open(WZ_file)) #load json file
    data_WZ = apply_br(data_WZ, br_z) #adjust data according to branching ratio
    workspace_WZ = make_workspace(data_WZ) 
    
    data_Wh = json.load(open(Wh_file)) #load json file
    data_Wh = apply_br(data_Wh, 1.0 - br_z) #adjust data according to branching ratio
    workspace_Wh = make_workspace(data_Wh)
    
    #rename modifiers in the WZ and Wh workspaces
    #This is correlation of modifiers.
    workspace_WZ,workspace_Wh = renaming_3l0l_workspaces(workspace_WZ,workspace_Wh)
    
    
    
    
    #Giving all signal samples the same name
    Z_sig_name = 'SMAwz13TeV_' + mass_point_string #prev. signal names
    h_sig_name = 'SMAwh13TeV_' + mass_point_string
    workspace_WZ = workspace_WZ.rename(samples = {Z_sig_name:'signal_sample'}) #renaming so one signal sample name throughout
    workspace_Wh = workspace_Wh.rename(samples = {h_sig_name:'signal_sample'})
    
    Z_sig_name = 'sig_SM_C1N2_WZ_' + mass_point_string
    h_sig_name = 'sig_SM_C1N2_Wh_' + mass_point_string
    workspace_WZ = workspace_WZ.rename(samples = {Z_sig_name: 'signal_sample'})
    workspace_Wh = workspace_Wh.rename(samples = {h_sig_name: 'signal_sample'}) 



    #need different measurement names when combining
    workspace_WZ = workspace_WZ.rename(measurements={'NormalMeasurement': 'WZ_Measurement'}) 
    workspace_Wh = workspace_Wh.rename(measurements={'NormalMeasurement': 'Wh_Measurement'})




    #Account for common channels
    overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    for k in range(len(overlap_channels)):
        channel_OI = overlap_channels[k] #selecting channel to apply overlap
        workspace_WZ,workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)
    #overlap sets workspace_WZ so that all workspace_Wh signal addition for the overlapping channel
    #of interest is in that channel for workspace WZ, then channel pruned from workspace_Wh
        
    workspace_Wh = pyhf.workspace.Workspace.prune(workspace_Wh, channels = overlap_channels)
    workspace_WZ = pyhf.workspace.Workspace.prune(workspace_WZ, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.
    


    workspace = pyhf.Workspace.combine(workspace_WZ,workspace_Wh)
    workspace = pyhf.Workspace.combine(workspace_bkgd_only_1lbb,workspace,join = 'left outer')
    workspace = pyhf.Workspace.combine(workspace,workspace_twolep,join = 'left outer') #########################################
    #have now combined all the workspaces
    
    
    #patching in the 1lbb signal data
    patch_name = 'C1N2_Wh_hbb_' + mass_point_string #which mass point signal contribution we are adding
    patchset = apply_br_patchset(patchset,1.0-br_z,mass_point) #apply the specifc branching ratio to the signal sample
    modifier_settings={
        "normsys": {"interpcode": "code4"},
        "histosys": {"interpcode": "code4p"},
    }
    model = workspace.model(
        patches=[patchset[patch_name]],
        modifier_settings=modifier_settings
    )
    

    
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output

def CLs_0l1lbb3l_combination(br, mass_point):
    
    br_z = br/100 #percentage branching ratio to decimal
    
    #turn mass_point into useable string
    m_light_neutralino = mass_point[1]; m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    

    #Loading in the 1lbb data and forming bkgd. workspace
    file1 = 'C1N2Wh_1Lbb_bkgonly.json' # file for the background samples
    file2 = 'C1N2Wh_1Lbb_patchset.json' #file for the signal patches
    
    spec = json.load(open(file1))
    patchset = pyhf.PatchSet( json.load(open(file2)) ) 
    workspace_bkgd_only_1lbb = pyhf.Workspace(spec) # background only workspace
    
    
    #Loading and adjusting the WZ/Wh data, which is premade to be a combination of 3l and 0l
# ....................
    file_0l_WZ = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_0l_Wh = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ;
    
    file_3l_WZ = 'C1N2WZ_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_3l_Wh = 'C1N2Wh_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json';

    data_0l_z = json.load(open(file_0l_WZ)) #loading data as dict object
    data_0l_h = json.load(open(file_0l_Wh))
    data_3l_z = json.load(open(file_3l_WZ)) #loading data as dict object
    data_3l_h = json.load(open(file_3l_Wh))

    data_0l_z = apply_br(data_0l_z, br_z)
    data_0l_h = apply_br(data_0l_h, 1.0 - br_z)
    data_3l_z = apply_br(data_3l_z, br_z)
    data_3l_h = apply_br(data_3l_h, 1.0 - br_z)
# ....................
    workspace_Wh_0l = make_workspace(data_0l_h)
    workspace_WZ_0l = make_workspace(data_0l_z)
    workspace_Wh_3l = make_workspace(data_3l_h)
    workspace_WZ_3l = make_workspace(data_3l_z)

#  ...................

    workspace_Wh_0l = workspace_Wh_0l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_0l'})
    workspace_WZ_0l = workspace_WZ_0l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_0l'})

    try:
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    except:
        # SMAwh13TeV_300.0_150.0
        mass_point_string_mod = str(m_heavy_linos)+'.0_'+str(m_light_neutralino)+'.0'
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string_mod:'signal_sample'}) 
        #want common signal sample name
    try:
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'})
    except:
        # SMAwh13TeV_300.0_150.0
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'CN_WZ_'+mass_point_string:'signal_sample'})
# ....................

    # overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    # for k in range(len(overlap_channels)):
    #     channel_OI = overlap_channels[k] #selecting channel to apply overlap
    #     workspace_WZ, workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)


    # workspace_Wh_0l = pyhf.workspace.Workspace.prune(workspace_Wh_0l, channels = overlap_channels)
    workspace_WZ_0l = pyhf.workspace.Workspace.prune(workspace_WZ_0l, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.

# ....................
    CR_channels = ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts'];  #control regions common, not adjoint
    #in the combined workspace want just one CR for each of the above, with the signal sample the combinaiton of the WZ and Wh signal samples
    for k in range(len(CR_channels)):
        channel_OI = CR_channels[k] 
        workspace_WZ_3l,workspace_Wh_3l = overlap(workspace_WZ_3l, workspace_Wh_3l, channel_OI) #all W2 signal sample data accounted for now in W1
    

    workspace_Wh_3l = pyhf.workspace.Workspace.prune(workspace_Wh_3l, channels = CR_channels)

# .................... 

    workspace_Wh_3l = workspace_Wh_3l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_3l'})
    workspace_WZ_3l = workspace_WZ_3l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_3l'})


    workspace_0l = pyhf.Workspace.combine(workspace_WZ_0l, workspace_Wh_0l)
    workspace_3l = pyhf.Workspace.combine(workspace_WZ_3l, workspace_Wh_3l)
    workspace = pyhf.Workspace.combine(workspace_0l, workspace_3l)

    workspace = pyhf.Workspace.combine(workspace_bkgd_only_1lbb, workspace, join = 'left outer')
    # workspace = pyhf.Workspace.combine(workspace, workspace_twolep, join = 'left outer') #########################################
    #have now combined all the workspaces
    
    
    #patching in the 1lbb signal data
    patch_name = 'C1N2_Wh_hbb_' + mass_point_string #which mass point signal contribution we are adding
    patchset = apply_br_patchset(patchset,1.0-br_z,mass_point) #apply the specifc branching ratio to the signal sample
    modifier_settings={
        "normsys": {"interpcode": "code4"},
        "histosys": {"interpcode": "code4p"},
    }
    model = workspace.model(
        patches=[patchset[patch_name]],
        modifier_settings=modifier_settings
    )
    

    
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output

def CLs_0l1lbb2l2j3l_combination(br, mass_point):
    
    br_z = br/100 #percentage branching ratio to decimal
    
    #turn mass_point into useable string
    m_light_neutralino = mass_point[1]; m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    
    mass_point_string_2l2j = str(m_heavy_linos)+'p0_'+str(m_light_neutralino)+'p0'


    # Loading 2L2J data

    file_2l2j_z = '2L2J_C1N2_WZ_' + mass_point_string_2l2j + '_2L2J.json'
    # file_2l2j_h = '2L2J_C1N2_Wh_' + mass_point_string_2l2j + '_2L2J.json'


    data_2l2j_z = json.load(open(file_2l2j_z))
    # data_2l2j_h = json.load(open(file_2l2j_h))

    data_2l2j_z = apply_br(data_2l2j_z, br_z) #modify signal samples in the data to account for branching ratio
    # data_2l2j_h = apply_br(data_2l2j_h, 1.0 - br_z)

    workspace_2l2j_z = make_workspace(data_2l2j_z) 
    # workspace_2l2j_h = make_workspace(data_2l2j_h) 



    #Loading in the 1lbb data and forming bkgd. workspace
    file1 = 'C1N2Wh_1Lbb_bkgonly.json' # file for the background samples
    file2 = 'C1N2Wh_1Lbb_patchset.json' #file for the signal patches
    
    spec = json.load(open(file1))
    patchset = pyhf.PatchSet( json.load(open(file2)) ) 
    workspace_bkgd_only_1lbb = pyhf.Workspace(spec) # background only workspace
    
    
    #Loading and adjusting the WZ/Wh data, which is premade to be a combination of 3l and 0l
# ....................

    file_0l_WZ = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_0l_Wh = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ;
    
    file_3l_WZ = 'C1N2WZ_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_3l_Wh = 'C1N2Wh_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json';

    data_0l_z = json.load(open(file_0l_WZ)) #loading data as dict object
    data_0l_h = json.load(open(file_0l_Wh))
    data_3l_z = json.load(open(file_3l_WZ)) #loading data as dict object
    data_3l_h = json.load(open(file_3l_Wh))

    data_0l_z = apply_br(data_0l_z, br_z)
    data_0l_h = apply_br(data_0l_h, 1.0 - br_z)
    data_3l_z = apply_br(data_3l_z, br_z)
    data_3l_h = apply_br(data_3l_h, 1.0 - br_z)
# ....................
    workspace_Wh_0l = make_workspace(data_0l_h)
    workspace_WZ_0l = make_workspace(data_0l_z)
    workspace_Wh_3l = make_workspace(data_3l_h)
    workspace_WZ_3l = make_workspace(data_3l_z)

#  ...................

    workspace_Wh_0l = workspace_Wh_0l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_0l'})
    workspace_WZ_0l = workspace_WZ_0l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_0l'})

    try:
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    except:
        # SMAwh13TeV_300.0_150.0
        mass_point_string_mod = str(m_heavy_linos)+'.0_'+str(m_light_neutralino)+'.0'
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string_mod:'signal_sample'}) 
        #want common signal sample name
    try:
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'})
    except:
        # SMAwh13TeV_300.0_150.0
        mass_point_string_mod = str(m_heavy_linos)+'.0_'+str(m_light_neutralino)+'.0' #want common signal sample name
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string_mod:'signal_sample'})

    # workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    # workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'})


# ....................

    # overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    # for k in range(len(overlap_channels)):
    #     channel_OI = overlap_channels[k] #selecting channel to apply overlap
    #     workspace_WZ, workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)


    # workspace_Wh_0l = pyhf.workspace.Workspace.prune(workspace_Wh_0l, channels = overlap_channels)
    workspace_WZ_0l = pyhf.workspace.Workspace.prune(workspace_WZ_0l, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.

# ....................
    CR_channels = ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts'];  #control regions common, not adjoint
    #in the combined workspace want just one CR for each of the above, with the signal sample the combinaiton of the WZ and Wh signal samples
    for k in range(len(CR_channels)):
        channel_OI = CR_channels[k] 
        workspace_WZ_3l,workspace_Wh_3l = overlap(workspace_WZ_3l, workspace_Wh_3l, channel_OI) #all W2 signal sample data accounted for now in W1
    

    workspace_Wh_3l = pyhf.workspace.Workspace.prune(workspace_Wh_3l, channels = CR_channels)

# .................... 

    workspace_Wh_3l = workspace_Wh_3l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_3l'})
    workspace_WZ_3l = workspace_WZ_3l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_3l'})


    workspace_0l = pyhf.Workspace.combine(workspace_WZ_0l, workspace_Wh_0l)
    workspace_3l = pyhf.Workspace.combine(workspace_WZ_3l, workspace_Wh_3l)
    workspace = pyhf.Workspace.combine(workspace_0l, workspace_3l)
    workspace = pyhf.Workspace.combine(workspace, workspace_2l2j_z, join = 'left outer')

    workspace = pyhf.Workspace.combine(workspace_bkgd_only_1lbb, workspace, join = 'left outer')
    # workspace = pyhf.Workspace.combine(workspace, workspace_twolep, join = 'left outer') #########################################
    #have now combined all the workspaces
    
    
    #patching in the 1lbb signal data
    patch_name = 'C1N2_Wh_hbb_' + mass_point_string #which mass point signal contribution we are adding
    patchset = apply_br_patchset(patchset,1.0-br_z,mass_point) #apply the specifc branching ratio to the signal sample
    modifier_settings={
        "normsys": {"interpcode": "code4"},
        "histosys": {"interpcode": "code4p"},
    }
    model = workspace.model(
        patches=[patchset[patch_name]],
        modifier_settings=modifier_settings
    )
    

    
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output


def CLs_0l2l2j_combination(br, mass_point):

    m_light_neutralino = mass_point[1]
    m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)

    mass_point_string_2l2j = str(m_heavy_linos)+'p0_'+str(m_light_neutralino)+'p0'
    
    br_z = br/100

    # Loading WZ 2L2J data

    file_2l2j_z = '2L2J_C1N2_WZ_' + mass_point_string_2l2j + '_2L2J.json'
    data_2l2j_z = json.load(open(file_2l2j_z))
    data_2l2j_z = apply_br(data_2l2j_z, br_z) #modify signal samples in the data to account for branching ratio
    workspace_2l2j_z = make_workspace(data_2l2j_z)
    # workspace_2l2j_z = pyhf.workspace.Workspace.prune(workspace_2l2j_z, channels = ['CR2B2Q_cuts']) 
    workspace_2l2j_z = workspace_2l2j_z.rename(measurements={'NormalMeasurement': 'WZ_2l2j_Measurement'})

    # workspace = pyhf.Workspace.combine(workspace_2l2j_z, workspace_2l2j_h) #combining Wh and WZ workspaces to form new combined workspace
    # .....................
#  WZ/Wh 0l data
    file_0l_WZ = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json'
    file_0l_Wh = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json'

    data_0l_z = json.load(open(file_0l_WZ)) #loading data as dict object
    data_0l_h = json.load(open(file_0l_Wh))

    data_0l_z = apply_br(data_0l_z, br_z)
    data_0l_h = apply_br(data_0l_h, 1.0 - br_z)

    workspace_Wh_0l = make_workspace(data_0l_h)
    workspace_WZ_0l = make_workspace(data_0l_z)

    workspace_Wh_0l = workspace_Wh_0l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_0l'})
    workspace_WZ_0l = workspace_WZ_0l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_0l'})

    workspace_WZ_0l = pyhf.workspace.Workspace.prune(workspace_WZ_0l, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.
    workspace_0l = pyhf.Workspace.combine(workspace_WZ_0l, workspace_Wh_0l)
    
    # workspace = pyhf.Workspace.combine(workspace_0l, workspace_2l2j_z, join = 'lseft outer') 
    workspace = pyhf.Workspace.combine(workspace_0l, workspace_2l2j_z)

    # ......................

    #note signal regions in this case are adjoint so don't need to account for overlap
    model = workspace.model()
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True) #generate CLs values, observed and expected bounds
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output



def CLs_0l3l_no_patch1lbb_combo(br, mass_point):
    
    br_z = br/100 #percentage branching ratio to decimal
    
    #turn mass_point into useable string
    m_light_neutralino = mass_point[1]; m_heavy_linos = mass_point[0]
    mass_point_string = str(m_heavy_linos)+'_'+str(m_light_neutralino)
    

    #Loading in the 1lbb Wh data from fille
    file_1lbb_Wh = 'C1N2Wh_1Lbb_'+ mass_point_string +'_combined_NormalMeasurement_model.json' 
    data_1lbb_h = json.load(open(file_1lbb_Wh))
    workspace_Wh_1lbb = make_workspace(data_1lbb_h)
    
    #Loading and adjusting the WZ/Wh data, which is premade to be a combination of 3l and 0l
# ....................
    file_0l_WZ = 'C1N2WZ_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_0l_Wh = 'C1N2Wh_0L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ;
    
    file_3l_WZ = 'C1N2WZ_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json' ; 
    file_3l_Wh = 'C1N2Wh_3L_'+ mass_point_string +'_combined_NormalMeasurement_model.json';

    data_0l_z = json.load(open(file_0l_WZ)) #loading data as dict object
    data_0l_h = json.load(open(file_0l_Wh))
    data_3l_z = json.load(open(file_3l_WZ)) #loading data as dict object
    data_3l_h = json.load(open(file_3l_Wh))

    data_0l_z = apply_br(data_0l_z, br_z)
    data_0l_h = apply_br(data_0l_h, 1.0 - br_z)
    data_3l_z = apply_br(data_3l_z, br_z)
    data_3l_h = apply_br(data_3l_h, 1.0 - br_z)
# ....................
    workspace_Wh_0l = make_workspace(data_0l_h)
    workspace_WZ_0l = make_workspace(data_0l_z)
    workspace_Wh_3l = make_workspace(data_3l_h)
    workspace_WZ_3l = make_workspace(data_3l_z)

#  ...................

    workspace_Wh_0l = workspace_Wh_0l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_0l'})
    workspace_WZ_0l = workspace_WZ_0l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_0l'})

    try:
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string:'signal_sample'}) #want common signal sample name
    except:
        # SMAwh13TeV_300.0_150.0
        mass_point_string_mod = str(m_heavy_linos)+'.0_'+str(m_light_neutralino)+'.0'
        workspace_Wh_3l = workspace_Wh_3l.rename(samples = {'SMAwh13TeV_'+mass_point_string_mod:'signal_sample'}) 
        #want common signal sample name
    try:
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'SMAwz13TeV_'+mass_point_string:'signal_sample'})
    except:
        # SMAwh13TeV_300.0_150.0
        workspace_WZ_3l = workspace_WZ_3l.rename(samples = {'CN_WZ_'+mass_point_string:'signal_sample'})
# ....................

    # overlap_channels =  ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts']; #common channels, don't want to double count
    # for k in range(len(overlap_channels)):
    #     channel_OI = overlap_channels[k] #selecting channel to apply overlap
    #     workspace_WZ, workspace_Wh = overlap(workspace_WZ,workspace_Wh,channel_OI)


    # workspace_Wh_0l = pyhf.workspace.Workspace.prune(workspace_Wh_0l, channels = overlap_channels)
    workspace_WZ_0l = pyhf.workspace.Workspace.prune(workspace_WZ_0l, channels = ['CR2B2Q_cuts']) #WZ has not signal sample for this channel so can prune.

# ....................
    CR_channels = ['WZ_CR_LowHT_cuts', 'WZ_CR_HighHT_cuts', 'WZ_CR_0jets_cuts'];  #control regions common, not adjoint
    #in the combined workspace want just one CR for each of the above, with the signal sample the combinaiton of the WZ and Wh signal samples
    for k in range(len(CR_channels)):
        channel_OI = CR_channels[k] 
        workspace_WZ_3l,workspace_Wh_3l = overlap(workspace_WZ_3l, workspace_Wh_3l, channel_OI) #all W2 signal sample data accounted for now in W1
    

    workspace_Wh_3l = pyhf.workspace.Workspace.prune(workspace_Wh_3l, channels = CR_channels)

# .................... 

    workspace_Wh_3l = workspace_Wh_3l.rename(measurements={'NormalMeasurement': 'Wh_Measurement_3l'})
    workspace_WZ_3l = workspace_WZ_3l.rename(measurements={'NormalMeasurement': 'WZ_Measurement_3l'})


    workspace_0l = pyhf.Workspace.combine(workspace_WZ_0l, workspace_Wh_0l)
    workspace_3l = pyhf.Workspace.combine(workspace_WZ_3l, workspace_Wh_3l)
    workspace = pyhf.Workspace.combine(workspace_0l, workspace_3l)
    workspace = pyhf.Workspace.combine(workspace_Wh_1lbb, workspace, join = 'left outer')
# ..............................................



# ..............................................
    

    model = workspace.model()  
    data = workspace.data(model)
    CLs_obs, CLs_exp = pyhf.infer.hypotest(1.0, data, model, qtilde=True, return_expected_set = True)
    
    output = {'CLs_obs':CLs_obs , 'CLs_exp_0sigma':CLs_exp[2] , 'CLs_exp_-1sigma':CLs_exp[1] , 'CLs_exp_+1sigma':CLs_exp[3] }
    return output


