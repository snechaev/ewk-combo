from CLs_functions import CLs_full_combination
"""
    Function gives the CLs values for the combined 3l, 0l, and 1lbb data, for a specified Branching ratio
    3l and 0l data files combined prior into a single file
    Combination of 3l, 0l, 1lbb. So applied correlation scheme, so that modifiers of the same uncertainties covaried
    
    Inputs:
        br = float, the branching ratio being tested, i.e. if br = 25% => 25% WZ, 75% Wh.     
        mass_point = array including the mass of the light neutralino and the mass of the heavy linos [mass of heavy linos (int), mass of neutralino (int)]
        
    Outputs:
        A dictionary with labelled data points:
            {'CLs_obs':,    'CLs_exp_0sigma':,     'CLs_exp_-1sigma':,      'CLs_exp_+1sigma': }

"""
# print("CLs_full_combination")
# br = 30 #50 percent WZ
# mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]


# out = CLs_full_combination(br,mass_point)
# print(out)





from CLs_functions import CLs_3l_combined 
"""
    Function gives the CLs values for the 3 lepton model, combining data for both the WZ and Wh scenarios
    
    This function combines the files:
        C1N2Wh_3L_'mass point i.e. 200_50'_combined_NormalMeasurement_model.json
        C1N2WZ_3L_'mass point i.e. 200_50'_combined_NormalMeasurement_model.json
    
    Inputs:
        br = float, the branching ratio being tested, i.e. if br = 25% => 25% WZ, 75% Wh.     
        mass_point = array including the mass of the light neutralino and the mass of the heavy linos [mass of heavy linos (int), mass of neutralino (int)]
        
    Outputs:
        A dictionary with labelled data points:
            {'CLs_obs':,    'CLs_exp_0sigma':,     'CLs_exp_-1sigma':,      'CLs_exp_+1sigma': }
    
    i.e.,
    
br = 30 #50 percent WZ
mass_point = [200,120] # i.e. the 200_50 mass point is [200,50]
print('CLs_3l_combined')
print(mass_point)
Output = CLs_3l_combined(br, mass_point)
print(Output)
"""





from CLs_functions import CLs_0l_combined
"""
    Function gives the CLs values for the 0 lepton model, combining data for both the WZ and Wh scenarios
    
    This function combines the files:
        C1N2Wh_0L_'mass point i.e. 200_50'_combined_NormalMeasurement_model.json
        C1N2WZ_0L_'mass point i.e. 200_50'_combined_NormalMeasurement_model.json
    
    Inputs:
        br = float, the branching ratio being tested, i.e. if br = 25% => 25% WZ, 75% Wh.     
        mass_point = array including the mass of the light neutralino and the mass of the heavy linos [mass of heavy linos (int), mass of neutralino (int)]
        
    Outputs:
        A dictionary with labelled data points:
            {'CLs_obs':,    'CLs_exp_0sigma':,     'CLs_exp_-1sigma':,      'CLs_exp_+1sigma': }
    
    i.e.,
    
"""

# print("CLs_0l_combined")
# br = 30 #50 percent WZ
# mass_point = [200, 50] # i.e. the 200_50 mass point is [200,50]
# Output = CLs_0l_combined(br, mass_point)
# print(Output)












from CLs_functions import CLs_1lbb
"""
    Function gives the CLs values for the 1lbb model. This model only has Wh data/ a Wh scenario.
    
    files used are: 
        C1N2Wh_1Lbb_bkgonly.json , which is used to construct a bkgd only workspace
        C1N2Wh_1Lbb_patchset.json , which is used to construct a with signal model, containing patches for a set of mass points.
    
    Inputs:    
        mass_point = array including the mass of the light neutralino and the mass of the heavy linos [mass of heavy linos (int), mass of neutralino (int)]
    
    Outputs:
        A dictionary with labelled data points:
            {'CLs_obs':,    'CLs_exp_0sigma':,     'CLs_exp_-1sigma':,      'CLs_exp_+1sigma': }
    
    i.e.,
    
    br = 50 #50 percent WZ
    mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]    
    out = CLs_1lbb([200,50])
    print(out)
"""

from CLs_functions import CLs_my_full_combination
# print("CLs_my_full_combination")
# br = 30 #50 percent WZ
# mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]
# print('mass_point:', mass_point)


# out = CLs_full_combination(br,mass_point)
# print(out)


from CLs_functions import CLs_0l3l_combo
# print("CLs_0l3l_combo")

# br = 30 #50 percent WZ
# mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]
# print('mass_point:', mass_point)


# out = CLs_0l3l_combo(br,mass_point)
# print(out)

import time
from CLs_functions import CLs_0l1lbb3l_combination
# print("CLs_0l1lbb3l_combination")

# br = 30 #50 percent WZ
# mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]
# print('mass_point:', mass_point)


# out = CLs_0l1lbb3l_combination(br,mass_point)
# print(out)

# from CLs_functions import CLs_2l2j_combined
# print("CLs_2l2j_combined")
# br = 30
# mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]
# print('mass_point:', mass_point)


from CLs_functions import CLs_0l2l2j_combination
print("CLs_0l2l2j_combination_mod")
br = 30
mass_point = [200,50] # i.e. the 200_50 mass point is [200,50]
print('mass_point:', mass_point)
print('br:', br)

<<<<<<< BRZH/Main.py
out = CLs_0l2l2j_combination(br, mass_point)
=======
time_st1 = time.time()
out = CLs_0l1lbb3l_combination(br,mass_point)
time_st2 = time.time()
>>>>>>> BRZH/Main.py
print(out)
print("run time:", time_st2 - time_st1, "sec")













